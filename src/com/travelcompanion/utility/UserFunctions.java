
package com.travelcompanion.utility;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.travelcompanion.MainActivity;

import android.util.Log;

public class UserFunctions {
	
	private JSONParser jsonParser;
	
	//private static String loginURL = "http://rate-exchange.appspot.com/currency?from=GBP&to=USD";
	//private static String registerURL = "http://ioptime.webuda.com/index.php";
	String url = "";
	String json = "";
	String[] currencyCode = 
  	  {
  		  "EUR",
  	      "USD",
  	      "MXN",
  	       "CAD",
  	      "CNY",
  	      "JPY",
  	      "SGD",
  	      "AWG",
  	      "NZD" ,
  	      "AUD",
  	      "EUR",
  	      "GBP" 
  	     
  	  };
	   String[] countriesName = {
	    		  "Italy",
	    	      "United States",
	    	      "Mexico", 
	    	      "Canada", 
	    	      "China", 
	    	      "Japan", 
	    	      "Singapore", 
	    	      "Aruba", 
	    	      "New Zealand",  
	    	      "Australia",  
	    	      "France", 
	    	      "Great Britian" 
	    	    };
	   JSONObject obj;
	// constructor
	public UserFunctions(){
		jsonParser = new JSONParser();
	}
	
	public String getValues(int i)
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
	//	params.add(new BasicNameValuePair("tag", "updatepassword"));
	//	params.add(new BasicNameValuePair("id", uid));
	//	params.add(new BasicNameValuePair("password", password));
		
		   try {
		/*********************To USD start**************************************/
			    url = "http://rate-exchange.appspot.com/currency?from="+currencyCode[i]+"&to=USD";
				 json = jsonParser.getStringFromUrl(url, params);
				 obj = new JSONObject(json);
			     String to1 = obj.getString("to");
		         String rate1 = obj.getString("rate");
		         String from1 = obj.getString("from");
		         Log.d("to", to1.toString());
		         Log.d("rate", rate1.toString());
		         Log.d("from", from1.toString());
		         MainActivity.db.UpdateCurrencyConversiontoUSD(countriesName[i], rate1.toString());
		/*************************To USD ends**************************/
		 
     	/*********************From USD start**************************************/
				    url = "http://rate-exchange.appspot.com/currency?from=USD&to="+currencyCode[i];
					 json = jsonParser.getStringFromUrl(url, params);
					 obj = new JSONObject(json);
				     String to2 = obj.getString("to");
			         String rate2 = obj.getString("rate");
			         String from2 = obj.getString("from");
			         Log.d("to", to2.toString());
			         Log.d("rate", rate2.toString());
			         Log.d("from", from2.toString());
			         MainActivity.db.UpdateCurrencyConversionfromUSD(countriesName[i], rate2.toString());
			/*************************From USD ends**************************/
			   
			
		   } catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		
	return json;
		
	}
	/*
	public JSONObject verifyemail(String username,String email){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "verifyemail"));
		params.add(new BasicNameValuePair("username", username));
		params.add(new BasicNameValuePair("email", email));
		
	JSONObject json = jsonParser.getJSONFromUrl(loginURL, params);
		
		return json;
	}
	public String selectalldepartments(){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "selectalldepartments"));
		
		String json = jsonParser.getStringFromUrl(loginURL, params);
		
		return json;
	}
	public String adminreply(String username){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "adminreply"));
		params.add(new BasicNameValuePair("username", username));
		
		
		String asd = jsonParser.getStringFromUrl(loginURL, params);
		
		return asd;
	}
	public String searchuser(){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "searchuser"));
	
		
		String asd = jsonParser.getStringFromUrl(loginURL, params);
		
		return asd;
	}

	
	public String selectalluserfromdep(String depname){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "selectalluserfromdep"));
		params.add(new BasicNameValuePair("depname", depname));
		
		String json = jsonParser.getStringFromUrl(loginURL, params);
		
		return json;
	}
	public JSONObject loginUser(String username, String password){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", login_tag));
		params.add(new BasicNameValuePair("username", username));
		params.add(new BasicNameValuePair("password", password));
		JSONObject json = jsonParser.getJSONFromUrl(loginURL, params);
		// return json
		// Log.e("JSON", json.toString());
		return json;
	}
	
	
	public JSONObject registerUser(String name, String email, String password,String spinervalue,String dname){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", register_tag));
		params.add(new BasicNameValuePair("name", name));
		params.add(new BasicNameValuePair("email", email));
		params.add(new BasicNameValuePair("password", password));
		params.add(new BasicNameValuePair("type", spinervalue));
		params.add(new BasicNameValuePair("dname", dname));
		
		// getting JSON Object
		JSONObject json = jsonParser.getJSONFromUrl(registerURL, params);
		// return json
		return json;
	}
	
	
	public JSONObject getgroupid(String name){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "getgroupid"));
		params.add(new BasicNameValuePair("gname", name));

		JSONObject json = jsonParser.getJSONFromUrl(loginURL, params);
		// return json
		// Log.e("JSON", json.toString());
		return json;
	}
	public JSONObject getmemberid(String name){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "getmemberid"));
		params.add(new BasicNameValuePair("name", name));

		JSONObject json = jsonParser.getJSONFromUrl(loginURL, params);
		// return json
		// Log.e("JSON", json.toString());
		return json;
	}
	public JSONObject adduseringroup(String gid,String oid,String mid,String gname,String oname,String mname){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "adduseringroup"));
		params.add(new BasicNameValuePair("gid", gid));
		params.add(new BasicNameValuePair("oid", oid));
		params.add(new BasicNameValuePair("mid", mid));
		params.add(new BasicNameValuePair("gname",gname));
		params.add(new BasicNameValuePair("oname", oname));
		params.add(new BasicNameValuePair("mname", mname));
		JSONObject json = jsonParser.getJSONFromUrl(loginURL, params);
		// return json
		// Log.e("JSON", json.toString());
		return json;
	}
	public JSONObject addgroup(String uid,String uname,String text){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "addgroup"));
		params.add(new BasicNameValuePair("uid", uid));
		params.add(new BasicNameValuePair("uname", uname));
		params.add(new BasicNameValuePair("gname", text));
		JSONObject json = jsonParser.getJSONFromUrl(loginURL, params);
		return json;
	}
	public JSONObject sendmestouser(String aname,String uname,String message){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "sendmestouser"));
		params.add(new BasicNameValuePair("aname", aname));
		params.add(new BasicNameValuePair("uname", uname));
		params.add(new BasicNameValuePair("message", message));
		JSONObject json = jsonParser.getJSONFromUrl(loginURL, params);
		// return json
		// Log.e("JSON", json.toString());
		return json;
	}
	public JSONObject adddepartment(String department){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "adddepartment"));
		params.add(new BasicNameValuePair("department", department));
		JSONObject json = jsonParser.getJSONFromUrl(loginURL, params);
		// return json
		// Log.e("JSON", json.toString());
		return json;
	}
	public JSONObject countallusers(){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "countallusers"));
		JSONObject json = jsonParser.getJSONFromUrl(loginURL, params);
		return json;
	}
	
	public String countallgroup(String uid,String uname){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "countallgroup"));
		params.add(new BasicNameValuePair("uid", uid));
		params.add(new BasicNameValuePair("uname", uname));
		
		String json = jsonParser.getStringFromUrl(loginURL, params);
		// return json
		// Log.e("JSON", json.toString());
		return json;
	}

	public String selectalluserexceptadmin(){
	
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "selectalluserexceptadmin"));
	//	params.add(new BasicNameValuePair("uid", uid));
		
		String json = jsonParser.getStringFromUrl(loginURL, params);
		
		return json;
	}
	public String selectallusertodelete(String uid){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "selectallusertodelete"));
		params.add(new BasicNameValuePair("uid", uid));
		
		String json = jsonParser.getStringFromUrl(loginURL, params);
		
		return json;
	}
	public String selectalluserexceptsender(String uid,String dname){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "selectalluserexceptsender"));
		params.add(new BasicNameValuePair("uid", uid));
		params.add(new BasicNameValuePair("dname",dname));
		
		String json = jsonParser.getStringFromUrl(loginURL, params);
		
		return json;
	}
	
	public String selectusersmessagesforadmin(String username){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "selectusersmessagesforadmin"));
		params.add(new BasicNameValuePair("username",username));
		
		String json = jsonParser.getStringFromUrl(loginURL, params);
		
		return json;
	}
	public String selectuserswhichsendsmes(){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "selectuserswhichsendsmes"));
		
		String json = jsonParser.getStringFromUrl(loginURL, params);
		
		return json;
	}
	public String selectallgroupmessages(String gid,String gname){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "selectallgroupmessages"));
		params.add(new BasicNameValuePair("gid", gid));
		params.add(new BasicNameValuePair("gname", gname));
		
		String json = jsonParser.getStringFromUrl(loginURL, params);
		
		return json;
	}
	public String selectallmessages(String gid){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "selectallmessages"));
		params.add(new BasicNameValuePair("gid", gid));
		
		String json = jsonParser.getStringFromUrl(loginURL, params);
		
		return json;
	}
	public String insertintogroup(String gid,String uid,String uname,String gname,String message){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "insertintogroup"));
		params.add(new BasicNameValuePair("gid", gid));
		params.add(new BasicNameValuePair("uid", uid));
		params.add(new BasicNameValuePair("uname", uname));
		params.add(new BasicNameValuePair("gname", gname));
		params.add(new BasicNameValuePair("message",message));
		//params.add(new BasicNameValuePair("time", time));
		
		String json = jsonParser.getStringFromUrl(loginURL, params);
		
		return json;
	}
	public JSONObject insertadmininbox(String uname,String mes){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "insertadmininbox"));
		params.add(new BasicNameValuePair("uname",uname));
		params.add(new BasicNameValuePair("message",mes));
		
		JSONObject json = jsonParser.getJSONFromUrl(loginURL, params);
		
		return json;
	}
	
	public String selectalldep(){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "selectalldep"));
		
		String json = jsonParser.getStringFromUrl(loginURL, params);
		// return json
		// Log.e("JSON", json.toString());
		return json;
	}
	public JSONObject checkmember(String gname,String mname){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "checkmember"));
		params.add(new BasicNameValuePair("gname",gname));
		params.add(new BasicNameValuePair("mname",mname));		
		JSONObject json = jsonParser.getJSONFromUrl(loginURL, params);
		// return json
		// Log.e("JSON", json.toString());
		return json;
	}
	public JSONObject selectallgroup(String i){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "selectallgroup"));
		params.add(new BasicNameValuePair("value",i));
		
		JSONObject json = jsonParser.getJSONFromUrl(loginURL, params);
		return json;
	}
	public JSONObject selectallusers(int i){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "selectallusers"));
		params.add(new BasicNameValuePair("value",String.valueOf(i)));
		JSONObject json = jsonParser.getJSONFromUrl(loginURL, params);
		return json;
	}
	public JSONObject  deletedepartment(String i){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "deletedepartment"));
		params.add(new BasicNameValuePair("deletename",i));
		
		JSONObject json = jsonParser.getJSONFromUrl(loginURL, params);
		// return json
		// Log.e("JSON", json.toString());
		return json;
	}
	public JSONObject  deletegroup(String i){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "deletegroup"));
		params.add(new BasicNameValuePair("deletename",i));
		
		JSONObject json = jsonParser.getJSONFromUrl(loginURL, params);
		// return json
		// Log.e("JSON", json.toString());
		return json;
	}
	public JSONObject  deleteuser(String i){
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "deleteuser"));
		params.add(new BasicNameValuePair("deletename",i));
		
		JSONObject json = jsonParser.getJSONFromUrl(loginURL, params);
		// return json
		// Log.e("JSON", json.toString());
		return json;
	}  */
}
