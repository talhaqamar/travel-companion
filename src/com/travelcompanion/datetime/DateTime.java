package com.travelcompanion.datetime;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.travelcompanion.DatabaseHelper;
import com.example.travelcompanion.MainActivity;
import com.fitscholar.travelcompanion.R;

public class DateTime extends Fragment {
	String origincountry="";
	String destinationcountry="";
	Spinner datetime_countries_origin,datetime_countries_destination;
	Spinner datetime_country1_subcity,datetime_country2_subcity;
	public TextView datetime_country1_time,datetime_country2_time,selected_countries_display_datetime,country1timezone,country2timezone;
	DatabaseHelper db=null;
	Button datetime_enterbtn;
	 MainActivity activity =null;
	 long miliSeconds1;
     SimpleDateFormat sdf1=null;
     Date resultdate1=null;
	 long miliSeconds2;
	 long test;
     SimpleDateFormat sdf2=null;
     Date resultdate2=null;
     private Calendar current1;
     private Calendar current2;
     String plusO="";
     String plusD="";
     int hrs1,mins1,hrs2,mins2;
	public DateTime() {

	    }
	    
	    	@Override
	    	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                Bundle savedInstanceState) {
	    //		 origincountry = getArguments().getString("origincountry");
	    //		destinationcountry = getArguments().getString("destinationcountry"); 
	          View view = inflater.inflate(R.layout.datetime_layout, container,
	                      false);
	         activity = (MainActivity) getActivity();
	          String origin = activity.getOrigin();
	          String destination = activity.getDestination();
	          int destinationposition = activity.getDestinationposition();
	          int originposition = activity.getOriginpostion();
	          
	         // Toast.makeText(activity,origin.toString()+","+destination.toString() ,5000).show();
	          initialize(view);
	          datetime_countries_origin.setSelection(originposition);
	          datetime_countries_origin.setEnabled(false);
	          datetime_countries_origin.setClickable(false);
	          datetime_countries_destination.setSelection(destinationposition);
	          datetime_countries_destination.setEnabled(false);
	          datetime_countries_destination.setClickable(false);
	          
	         // List<AllEntries> country = db.getSelectedEntries(origin);
	         List<String> originsubcities = db.getAllEntriesString(origin);
	  		ArrayAdapter<String> originAdapter = new ArrayAdapter<String>(activity,
	  				android.R.layout.simple_spinner_item, originsubcities);
	  		originAdapter
	  				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	  		datetime_country1_subcity.setAdapter(originAdapter);
	  		 
	  		List<String> destinationsubcities = db.getAllEntriesString(destination);
		  		ArrayAdapter<String> destinationAdapter = new ArrayAdapter<String>(activity,
		  				android.R.layout.simple_spinner_item, destinationsubcities);
		  		destinationAdapter
		  				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		  		datetime_country2_subcity.setAdapter(destinationAdapter);
		  		
		  		List<String> origintimezone = db.checktimezone(origin); 
		  		List<String> destinationtimezone = db.checktimezone(destination); 
			  	
		  		if(origintimezone.get(0).toString() == "Yes" || origintimezone.get(0).toString().contains("Yes"))
		  		{    country1timezone.setText("This country has multiple time zones. Please choose a location.");}
			  	else if(origintimezone.get(0).toString() == "No" || origintimezone.get(0).toString().contains("No")) { country1timezone.setText("This country has one timezone."); }

		  		if(destinationtimezone.get(0).toString() == "Yes" || destinationtimezone.get(0).toString().contains("Yes")){
		  			country2timezone.setText("This country has multiple time zones. Please choose a location.");}
			  	else if(destinationtimezone.get(0).toString() == "No" || destinationtimezone.get(0).toString().contains("No")){ country2timezone.setText("This country has one timezone."); }
			  	
			  	
		  		if(origin.length() >1 && destination.length() >1) 
		  		{
		  			selected_countries_display_datetime.setText(origin + " - > " + destination);
		  		}
		  		else 
		  		{
		  			selected_countries_display_datetime.setText("Choose Country" + " - > " + "Choose Country");
				  			
		  		}
		  		datetime_enterbtn.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						
						sdf1 = new SimpleDateFormat("EEEE, dd MMMM yyyy HH:mm:ss");	
						sdf2 = new SimpleDateFormat("EEEE, dd MMMM yyyy HH:mm:ss");	
						
						datetime_country1_time.setText("");
						datetime_country2_time.setText("");
					String originselected = datetime_country1_subcity.getSelectedItem().toString();
					String destinationselected = datetime_country2_subcity.getSelectedItem().toString();
					
						List<String> originID = db.getstring(originselected);
						List<String> destinationID = db.getstring(destinationselected);
					
					String o = originID.get(0).toString();
					String d = destinationID.get(0).toString();
					String originplus = db.getstringplus(originselected);
					String destinationplus = db.getstringplus(destinationselected);
					
					plusO = originplus.toString();
					plusD = destinationplus.toString();
					
					Log.d("plusO",plusO.toString());

					Log.d("plusO",plusO.toString());
					Log.d("plusD",plusD.toString());
					getGMTTimeforORIGIN();
					getGMTTimeforDESTINATION();
					
					TimeZone timezone1 = TimeZone.getTimeZone(o);
					String TimeZoneName1 = timezone1.getDisplayName();

					int TimeZoneOffset1 = timezone1.getRawOffset()
							/ (60 * 1000 );

					 hrs1 = TimeZoneOffset1 / 60 ;
					 mins1 = TimeZoneOffset1 % 60;
					
					if(plusO.equals("3600000") ||  plusO.contains("3600000"))
					{ hrs1 = hrs1+1;}
					
					miliSeconds1 = miliSeconds1 + Long.parseLong(plusO) + timezone1.getRawOffset();
					Log.d("miliseconds1",String.valueOf(miliSeconds1));
					resultdate1 = new Date(miliSeconds1);
					System.out.println(sdf1.format(resultdate1));
					datetime_country1_time.setTextSize(14.0f);
					datetime_country1_time.setText(TimeZoneName1 + " : GMT " + hrs1 + "."
							+ mins1+ "\n" + sdf1.format(resultdate1));
					miliSeconds1 = 0;
					
					TimeZone timezone2 = TimeZone.getTimeZone(d);
					String TimeZoneName2 = timezone2.getDisplayName();

					int TimeZoneOffset2 = timezone2.getRawOffset()
							/ (60 * 1000 );

					 hrs2 = TimeZoneOffset2 / 60 ;
					 mins2 = TimeZoneOffset2 % 60;
					 if(plusD.equals("3600000") ||  plusD.contains("3600000"))
						{ hrs2 = hrs2+1;}
					miliSeconds2 = miliSeconds2 + Long.parseLong(plusD) + timezone2.getRawOffset();
					Log.d("miliseconds2",String.valueOf(miliSeconds2));
					resultdate2 = new Date(miliSeconds2);
					System.out.println(sdf2.format(resultdate2));
					datetime_country2_time.setTextSize(14.0f);
					datetime_country2_time.setText(TimeZoneName2 + " : GMT " + hrs2 + "."
							+ mins2+ "\n" + sdf2.format(resultdate2));
					miliSeconds2 = 0;
					
				/*	TimeZone timezone2 = TimeZone.getTimeZone(d);
					String TimeZoneName2 = timezone2.getDisplayName();

					int TimeZoneOffset2 = timezone2.getRawOffset()
							/ (60 * 1000);

					int hrs2 = (TimeZoneOffset2 / 60);
					System.out.println("timezoneoffset2/60 :"+ String.valueOf(TimeZoneOffset2 / 60));
					System.out.println("hrs 2:"+ String.valueOf(hrs2));
					
					int mins2 = TimeZoneOffset2 % 60;

					miliSeconds2 = miliSeconds2 + Long.parseLong(plusD)  + timezone2.getRawOffset() ;
					
					resultdate2 = new Date(miliSeconds2);
					System.out.println(sdf2.format(resultdate2));
					datetime_country2_time.setTextSize(14.0f);
					datetime_country2_time.setText(TimeZoneName2 + " : GMT " + hrs2 + "."
							+ mins2+ "\n" + sdf2.format(resultdate2));
					miliSeconds2 = 0;
				*/	}
				});
		  			
		  			return view;
	    
	}
	    	private void getGMTTimeforORIGIN() {
	    		current1 = Calendar.getInstance();
	    		//txtCurrentTime.setText("" + current.getTime());

	    		miliSeconds1 = current1.getTimeInMillis();

	    		TimeZone tzCurrent = current1.getTimeZone();
	    		int offset = tzCurrent.getRawOffset();
	    		if (tzCurrent.inDaylightTime(new Date())) {
	    			offset = offset + tzCurrent.getDSTSavings();
	    		}

	    		miliSeconds1 = miliSeconds1  - offset;

	    		resultdate1 = new Date(miliSeconds1);
	    		System.out.println(sdf1.format(resultdate1));
	    	}

	    	private void getGMTTimeforDESTINATION() {
	    		current2 = Calendar.getInstance();
	    		//txtCurrentTime.setText("" + current.getTime());

	    		miliSeconds2 = current2.getTimeInMillis();

	    		TimeZone tzCurrent = current2.getTimeZone();
	    		int offset = tzCurrent.getRawOffset();
	    		if (tzCurrent.inDaylightTime(new Date())) {
	    			offset = offset + tzCurrent.getDSTSavings();
	    		}

	    		miliSeconds2 = miliSeconds2  - offset;

	    		resultdate2 = new Date(miliSeconds2);
	    		System.out.println(sdf2.format(resultdate2));
	    	}
			private void initialize(View v)
			{
				 datetime_countries_origin = (Spinner)v.findViewById(R.id.datetime_countries_origin);
				 datetime_countries_destination = (Spinner)v.findViewById(R.id.datetime_countries_destination);
				 datetime_country1_subcity = (Spinner)v.findViewById(R.id.datetime_country1_subcity);
				 datetime_country2_subcity = (Spinner)v.findViewById(R.id.datetime_country2_subcity);
				 datetime_country1_time = (TextView)v.findViewById(R.id.datetime_country1_time);
				 datetime_country2_time = (TextView)v.findViewById(R.id.datetime_country2_time);
				 selected_countries_display_datetime= (TextView)v.findViewById(R.id.selected_countries_display_datetime);
				 datetime_enterbtn = (Button)v.findViewById(R.id.datetime_enterbtn);
				 country1timezone = (TextView)v.findViewById(R.id.country1timezone);
				 country2timezone = (TextView)v.findViewById(R.id.country2timezone);
				 db = new DatabaseHelper(v.getContext());
			}
}
