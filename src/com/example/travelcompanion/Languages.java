package com.example.travelcompanion;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.fitscholar.travelcompanion.R;

/* This class is a display class that shows common languages for both the origin and destination country  */ 

public class Languages extends Fragment {
	//the following are viewtype items  
    TextView languages_origin;
    TextView languages_origin_description;
    TextView languages_destination;
    TextView languages_destination_description;
    TextView languages_display;




    public Languages() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {

          View view = inflater.inflate(R.layout.languages_layout, container,
                      false);

          //the following makes all viewtype variables visable in the feild 
          languages_origin = (TextView) view.findViewById(R.id.languages_origin);
          languages_origin_description = (TextView) view.findViewById(R.id.languages_origin_description);
          languages_destination = (TextView) view.findViewById(R.id.languages_destination);
          languages_destination_description = (TextView) view.findViewById(R.id.languages_destination_description);
          languages_display = (TextView) view.findViewById(R.id.languages_display);

          //the following code gets the origin and destination from this fragments activity, MainActivity
          MainActivity activity = (MainActivity) getActivity();
          String origin = activity.getOrigin();
          String destination = activity.getDestination();
          DatabaseHelper dbHelper = new DatabaseHelper(activity);
          
          
          //this code sets viewable text views to desired output
          languages_origin.setText(origin);
          languages_destination.setText(destination);
          String originLanguage = dbHelper.getOriginLanguages();
          String destinationLanguage = dbHelper.getDestinationLanguages();
          languages_origin_description.setText(originLanguage);
          languages_destination_description.setText(destinationLanguage);
          languages_display.setText(origin + " - > " + destination);
          
          return view;
    }

}