package com.example.travelcompanion;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.fitscholar.travelcompanion.R;

/* This class is a future add on to be implemented.  The idea behind it is to allow users to customize the app
 * they could set a standard origin country, change the launch page etc. */ 

//Right now the page currently displays the icon and name associated with Customize drawer

public class Customize extends Fragment {
	 
    ImageView wtc_customize;
    TextView customize_text;


    public Customize() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {

          View view = inflater.inflate(R.layout.customize_layout, container,
                      false);
          
          wtc_customize = (ImageView) view.findViewById(R.id.wtc_customize);
          customize_text = (TextView) view.findViewById(R.id.customize_text);
          
         
          return view;
    }

}
