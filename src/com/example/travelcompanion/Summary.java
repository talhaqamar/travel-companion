package com.example.travelcompanion;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.fitscholar.travelcompanion.R;

/* This class is a display class that shows all fields for both the origin and destination country  */

public class Summary extends Fragment {
	
	//the following are viewtype items 
    TextView summary_origin;
    TextView summary_destination;
    TextView summary_currency;
    TextView summary_origin_currency;
    TextView summary_destination_currency;
    TextView summary_temperature;
    TextView summary_origin_temperature;
    TextView summary_destination_temperature;
    TextView summary_language;
    TextView summary_origin_language;
    TextView summary_destination_language;
    TextView summary_plug_type;
    TextView summary_origin_plug_type;
    TextView summary_destination_plug_type;
    TextView summary_measurements;
    TextView summary_origin_measurements;
    TextView summary_destination_measurements;
    TextView summary_display;




    public Summary() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {

    	final MainActivity activity = (MainActivity) getActivity();
    	
          View view = inflater.inflate(R.layout.summary_layout, container,
                      false);

          //the following makes all viewtype variables visable in the feild 
          summary_origin = (TextView) view.findViewById(R.id.summary_origin);
          summary_destination = (TextView) view.findViewById(R.id.summary_destination);
          summary_currency = (TextView) view.findViewById(R.id.summary_currency);
          summary_currency.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  activity.SelectItem(2); 
        	          }
        	});
          summary_origin_currency = (TextView) view.findViewById(R.id.summary_origin_currency);
          summary_origin_currency.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  activity.SelectItem(2); 
        	          }
        	});
          summary_destination_currency = (TextView) view.findViewById(R.id.summary_destination_currency);
          summary_destination_currency.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  activity.SelectItem(2); 
        	          }
        	});
          summary_temperature = (TextView) view.findViewById(R.id.summary_temperature);
          summary_temperature.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  activity.SelectItem(3); 
        	          }
        	});
          summary_origin_temperature = (TextView) view.findViewById(R.id.summary_origin_temperature);
          summary_origin_temperature.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  activity.SelectItem(3); 
        	          }
        	});
          summary_destination_temperature = (TextView) view.findViewById(R.id.summary_destination_temperature);
          summary_destination_temperature.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  activity.SelectItem(3); 
        	          }
        	});
          summary_language = (TextView) view.findViewById(R.id.summary_language);
          summary_language.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  activity.SelectItem(4); 
        	          }
        	});
          summary_origin_language = (TextView) view.findViewById(R.id.summary_origin_language);
          summary_origin_language.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  activity.SelectItem(4); 
        	          }
        	});
          summary_destination_language = (TextView) view.findViewById(R.id.summary_destination_language);
          summary_destination_language.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  activity.SelectItem(4); 
        	          }
        	});
          summary_plug_type = (TextView) view.findViewById(R.id.summary_plug_type);
          summary_plug_type.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  activity.SelectItem(5); 
        	          }
        	});
          summary_origin_plug_type = (TextView) view.findViewById(R.id.summary_origin_plug_type);
          summary_origin_plug_type.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  activity.SelectItem(5); 
        	          }
        	});
          summary_destination_plug_type = (TextView) view.findViewById(R.id.summary_destination_plug_type);
          summary_destination_plug_type.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  activity.SelectItem(5); 
        	          }
        	});
          summary_measurements = (TextView) view.findViewById(R.id.summary_measurements);
          summary_measurements.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  activity.SelectItem(6); 
        	          }
        	});
          summary_origin_measurements = (TextView) view.findViewById(R.id.summary_origin_measurements);
          summary_origin_measurements.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  activity.SelectItem(6); 
        	          }
        	});
          summary_destination_measurements = (TextView) view.findViewById(R.id.summary_destination_measurements);
          summary_destination_measurements.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  activity.SelectItem(6); 
        	          }
        	});
          summary_display = (TextView) view.findViewById(R.id.summary_display);

          //the following code gets the origin and destination from this fragments activity, MainActivity
          String origin = activity.getOrigin();
          String destination = activity.getDestination();
          
          //this code sets viewable text views to desired output
          summary_origin.setText(origin);
          summary_destination.setText(destination);
          summary_display.setText(origin + " - > " + destination);
          
          //the following code gets information from DatabaseHelper
          DatabaseHelper dbHelper = new DatabaseHelper(activity);
          String originCurrencyType = dbHelper.getOriginCurrencyType();
          String destinationCurrencyType = dbHelper.getDestinationCurrencyType();
          String originCurrencyConversion = dbHelper.getOriginCurrencyConversion();
          String destinationCurrencyConversion = dbHelper.getDestinationCurrencyConversion();
          String originTemperature = dbHelper.getOriginTemperature();
          String destinationTemperature = dbHelper.getDestinationTemperature();
          String originLanguages = dbHelper.getOriginLanguages();
          String destinationLanguages = dbHelper.getDestinationLanguages();
          String originPlugType = dbHelper.getOriginPlugType();
          String destinationPlugType = dbHelper.getDestinationPlugType();
          String originPlugTypeVolt = dbHelper.getOriginPlugTypeVolt();
          String destinationPlugTypeVolt = dbHelper.getDestinationPlugTypeVolt();
          String originPlugTypeFreq = dbHelper.getOriginPlugTypeFreq();
          String destinationPlugTypeFreq = dbHelper.getDestinationPlugTypeFreq();
          String originMeasurements = dbHelper.getOriginMeasurements();
          String destinationMeasurements = dbHelper.getDestinationMeasurements();
          
          summary_origin_currency.setText(originCurrencyType);
          summary_destination_currency.setText(destinationCurrencyType);
          summary_origin_temperature.setText(originTemperature);
          summary_destination_temperature.setText(destinationTemperature);
          summary_origin_language.setText(originLanguages);
          summary_destination_language.setText(destinationLanguages);
          summary_origin_plug_type.setText(originPlugType + " \n" + originPlugTypeVolt + "\n "+ originPlugTypeFreq);
          summary_destination_plug_type.setText(destinationPlugType + "\n " + destinationPlugTypeVolt + "\n "+ destinationPlugTypeFreq);
          summary_origin_measurements.setText(originMeasurements);
          summary_destination_measurements.setText(destinationMeasurements);

          
          return view;
    }

}