package com.example.travelcompanion;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.fitscholar.travelcompanion.R;

/* This class is a future add on to be implemented.  The idea behind it is to allow users to suggest countries to add
 * that would then be accepted or denied from the client.  The page would have a log in feature where potential 
 * manipulation could take place of the database. */ 

// Right now the page currently displays the icon and name associated with Admin drawer

public class Admin extends Fragment {
	 
    ImageView wtc_admin;
    TextView admin_text;

    Admin() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {

          View view = inflater.inflate(R.layout.admin_layout, container,
                      false);
          
          wtc_admin = (ImageView) view.findViewById(R.id.wtc_admin);
          admin_text = (TextView) view.findViewById(R.id.admin_text);
        
          return view;
    }

}