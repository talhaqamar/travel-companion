package com.example.travelcompanion;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.fitscholar.travelcompanion.R;

/* This class is a display class that shows common measuremtns for both the origin and destination country  */ 

public class Measurements extends Fragment  implements OnClickListener,OnItemClickListener{
	
	//the following are viewtype items  
    TextView measurements_origin;
    TextView measurements_destination;
    TextView measurements_origin_description;
    TextView measurements_destination_description;
    TextView measurements_display;
/****************Second iteration*************************************/
    
    TextView firsttextview,secondtextview;
    EditText firstedit,secondedit;
    Button enterbtn,clearbtn;
    Spinner mspinner;
    String fedittext="";
    String sedittext="";
    String spinnervalue="";
    DatabaseHelper db= null;
    public Measurements() {

    }
    
    	@Override
    	public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {

          View view = inflater.inflate(R.layout.measurements_layout, container,
                      false);

          //the following makes all viewtype variables visable in the feild 
          initialize(view);      
          firsttextview.setOnClickListener(this);
          secondtextview.setOnClickListener(this);
          firstedit.setOnClickListener(this);
          secondedit.setOnClickListener(this);
          enterbtn.setOnClickListener(this); 
          clearbtn.setOnClickListener(this);
          mspinner.setOnItemSelectedListener(new OnItemSelectedListener() {

              @Override
              public void onItemSelected(AdapterView<?> arg0, View arg1,
                      int arg2, long arg3) {
					String spinvalue = mspinner.getSelectedItem().toString();
					String[] separated = spinvalue.split("to/from");
					firsttextview.setText("");
					secondtextview.setText("");
					firsttextview.setText("Enter "+separated[0].trim().toString());
					secondtextview.setText("Enter "+separated[1].trim().toString());
					 }

              @Override
              public void onNothingSelected(AdapterView<?> arg0) {
                  // TODO Auto-generated method stub

              }
          });
          
         //the following code gets the origin and destination from this fragments activity, MainActivity
          MainActivity activity = (MainActivity) getActivity();
          String origin = activity.getOrigin();
          String destination = activity.getDestination();
          
          DatabaseHelper dbHelper = new DatabaseHelper(activity);
          String oMeasureString = dbHelper.getOriginCommonMeasure();
          String dMeasureString = dbHelper.getDestinationCommonMeasure();
          
          String[] oIndMeasure = oMeasureString.split(",");
          String[] dIndMeasure = dMeasureString.split(",");
          String originMeasures = "";
          String destinationMeasures = "";
          
          for(int i =0; i < oIndMeasure.length; i++){
        	  originMeasures = originMeasures + oIndMeasure[i] +"\n";
        	  destinationMeasures = destinationMeasures + dIndMeasure[i]+"\n";
          }
          
          //this code sets viewable text views to desired output
          measurements_origin.setText(origin);
          measurements_destination.setText(destination);
          measurements_origin_description.setText(originMeasures);
          measurements_destination_description.setText(destinationMeasures);
          measurements_display.setText(origin + " - > " + destination);

          return view;
    }


		private void initialize(View view) {
		      measurements_origin = (TextView) view.findViewById(R.id.measurements_origin);
	          measurements_destination = (TextView) view.findViewById(R.id.measurements_destination);
	          measurements_origin_description = (TextView) view.findViewById(R.id.measurements_origin_description);
	          measurements_destination_description = (TextView) view.findViewById(R.id.measurements_destination_description);
	          measurements_display = (TextView) view.findViewById(R.id.measurements_display);
	    	mspinner  = (Spinner)view.findViewById(R.id.measurementspinner);
	          /***************my code**************/

	          firsttextview = (TextView) view.findViewById(R.id.firsttextview);
	          secondtextview= (TextView) view.findViewById(R.id.secondtextview);
	          firstedit = (EditText) view.findViewById(R.id.firstedit);
	          secondedit = (EditText) view.findViewById(R.id.secondedit);
	          enterbtn = (Button)view.findViewById(R.id.enterbtn);
	          clearbtn= (Button)view.findViewById(R.id.clearbtn);
	         db = new DatabaseHelper(view.getContext());
		}

		@Override
		public void onClick(View v) {
			MainActivity activity = (MainActivity) getActivity();
	        switch (v.getId()) {
			 
			case R.id.firsttextview:
		         
				Toast.makeText(activity,"firsttextEnter" , 5000).show();
				break;
			
			case R.id.secondtextview:
				Toast.makeText(activity,"secondtextenter" , 5000).show();				
				break;
			
			case R.id.enterbtn:
     	        fedittext = firstedit.getText().toString();
     	         sedittext = secondedit.getText().toString();
				if(fedittext.length() == 0 && sedittext.length() == 0)
				{
					Toast.makeText(activity,"Please enter value in any of above input fields." , 5000).show();
				}
				else if((fedittext.length() > 0 && sedittext.length() == 0) || (fedittext.length()== 0 && sedittext.length() > 0))
				{
					if(fedittext.length() > 0)
					{  	//feet to meter	
						spinnervalue = mspinner.getSelectedItem().toString();
						String[] split = spinnervalue.split("to/from");
						String newString = split[0].trim().toString()+" to "+split[1].trim().toString();
						String getstring = db.getName1toName2(newString);
						Toast.makeText(activity,getstring.toString() , 5000).show();
						db.close();
						spinnervalue = "";	
						Double intgetvalue = Double.parseDouble(getstring.toString());
						Double intoriginal = Double.parseDouble(fedittext.toString());
						Double result  = intgetvalue * intoriginal;
						secondedit.setText(String.valueOf(result));
					}	
					else if(sedittext.length() > 0) 
					{
					spinnervalue = mspinner.getSelectedItem().toString();
					String[] split = spinnervalue.split("to/from");
					String newString1 = split[1].trim().toString()+" to "+split[0].trim().toString();
					String getstring1 = db.getName2toName1(newString1);
					db.close();
					Toast.makeText(activity,getstring1.toString() , 5000).show();
					spinnervalue = "";		
					Double intgetvalue1 = Double.parseDouble(getstring1.toString());
					Double intoriginal1 = Double.parseDouble(sedittext.toString());
					Double result1  = intgetvalue1 * intoriginal1;
					firstedit.setText(String.valueOf(result1));
					}
				}
				else if(fedittext.length() > 0 && sedittext.length() > 0)
				{
					Toast.makeText(activity,"One field should be empty" , 5000).show();
				}
     	       break;
			case R.id.clearbtn:
				firstedit.setText("");
				secondedit.setText("");
				Toast.makeText(activity,"Data Cleared in fields" , 5000).show();
				break;
			default:
				break;
			}
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			
		}

}