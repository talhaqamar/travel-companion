package com.example.travelcompanion;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.fitscholar.travelcompanion.R;

public class NextPlugTypes extends Fragment {
	TextView back;
	Activity activity;
	public NextPlugTypes() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.next_plug_layout, container,
                  false);
     	activity = (MainActivity) getActivity();
     	back = (TextView)view.findViewById(R.id.next_plug_layout_back);
  	
      String text  = "<html><a href=''>Back</a></html>";
      back.setText(Html.fromHtml(text));
      back.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
		
			Fragment fragment= new PlugTypes();
	        FragmentManager frgManager = getFragmentManager();
	            frgManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
	
		}
	});
      
      
		return view;
	}
}
