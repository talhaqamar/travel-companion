package com.example.travelcompanion;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.fitscholar.travelcompanion.R;
import com.travelcompanion.datetime.DateTime;
import com.travelcompanion.utility.AllEntries;
import com.travelcompanion.utility.UserFunctions;
 
public class MainActivity extends Activity {
 
      private DrawerLayout mDrawerLayout;
      private ListView mDrawerList;
      private ActionBarDrawerToggle mDrawerToggle;
 
      private CharSequence mDrawerTitle;
      private CharSequence mTitle;
      CustomDrawerAdapter adapter;
      private static String origin;
      private static String destination;
      private static int originposition;
      private static int destinationposition;
      ProgressDialog progress;
      List<DrawerItem> dataList;
     public  static DatabaseHelper db=null;
      /* */
     public int position =100;
      String[] currencyCode = 
    	  {
    		  "EUR",
    	      "USD",
    	      "MXN",
    	       "CAD",
    	      "CNY",
    	      "JPY",
    	      "SGD",
    	      "AWG",
    	      "NZD" ,
    	      "AUD",
    	      "EUR",
    	      "GBP" 
    	     
    	  };
      String[] countriesName = {
    		  "Italy",
    	      "United States",
    	      "Mexico", 
    	      "Canada", 
    	      "China", 
    	      "Japan", 
    	      "Singapore", 
    	      "Aruba", 
    	      "New Zealand",  
    	      "Australia",  
    	      "France", 
    	      "Great Britian" 
    	    };
      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
 
            // Initializing
            dataList = new ArrayList<DrawerItem>();
            mTitle = mDrawerTitle = getTitle();
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mDrawerList = (ListView) findViewById(R.id.left_drawer);
            db = new DatabaseHelper(getApplicationContext());
            mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                        GravityCompat.START);
 
            // Add Drawer Item to dataList
            dataList.add(new DrawerItem("Select Countries", R.drawable.globe));
            dataList.add(new DrawerItem("Summary", R.drawable.summary));
            dataList.add(new DrawerItem("Currency", R.drawable.currency));
            dataList.add(new DrawerItem("Temperature", R.drawable.temperatures));
            dataList.add(new DrawerItem("Languages", R.drawable.languages));
            dataList.add(new DrawerItem("Plug Types", R.drawable.plug));
            dataList.add(new DrawerItem("Measurements", R.drawable.measurements));
            dataList.add(new DrawerItem("Date/Time", R.drawable.datetime));//7
            dataList.add(new DrawerItem("Refresh", R.drawable.refresh)); //8
            //dataList.add(new DrawerItem("Customize", R.drawable.settings));
            //dataList.add(new DrawerItem("Admin", R.drawable.admin));
            
 
            adapter = new CustomDrawerAdapter(this, R.layout.custom_drawer_item,
                        dataList);
 
            mDrawerList.setAdapter(adapter);
 
            mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
 
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setHomeButtonEnabled(true);
 
            mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                        R.drawable.ic_drawer, R.string.drawer_open,
                        R.string.drawer_close) {
                  public void onDrawerClosed(View view) {
                        getActionBar().setTitle(mTitle);
                        invalidateOptionsMenu(); // creates call to
                                                                  // onPrepareOptionsMenu()
                  }
 
                  public void onDrawerOpened(View drawerView) {
                        getActionBar().setTitle(mDrawerTitle);
                        invalidateOptionsMenu(); // creates call to
                                                                  // onPrepareOptionsMenu()
                  }
            };
 
            mDrawerLayout.setDrawerListener(mDrawerToggle);
 
            if (savedInstanceState == null) {
                  SelectItem(0);
            }
      //   new LongOperation().execute("");
     
   /*  List<AllEntries> aruba = db.getSelectedEntries("Aruba");
     List<AllEntries> australia = db.getSelectedEntries("Australia");
     List<AllEntries> canada = db.getSelectedEntries("Canada");
     List<AllEntries> china = db.getSelectedEntries("China");
     List<AllEntries> france = db.getSelectedEntries("France");
     List<AllEntries> britian = db.getSelectedEntries("Great Britian");
     List<AllEntries> italy = db.getSelectedEntries("Italy");
     List<AllEntries> japan = db.getSelectedEntries("Japan");
     List<AllEntries> mexico = db.getSelectedEntries("Mexico");
     List<AllEntries> newzealand = db.getSelectedEntries("New Zealand");
     List<AllEntries> singapore = db.getSelectedEntries("Singapore");
     List<AllEntries> america = db.getSelectedEntries("America");
      */
      }
      private class LongOperation extends AsyncTask<String, Void, String> {

          @Override
          protected String doInBackground(String... params) {
        	  int i = Integer.valueOf(params[0]);
        	  
        	  UserFunctions u = new UserFunctions();
        	  	
             
			try {
				  String json = u.getValues(i); //jParser.getJSONFromUrl(url);
		 // Toast.makeText(MainActivity.this,json.toString(), 10000).show();
			Log.d("return data",json.toString());
		/* JSONObject obj;
				   obj = new JSONObject(json);
				   String to = obj.getString("to");
	               String rate = obj.getString("rate");
	               String from = obj.getString("from");
	              // Toast.makeText(MainActivity.this,"to"+to.toString()+"\n"+"rate"+rate.toString()+"\n"+"from"+from.toString()+"\n", 5000).show();
	               Log.d("to", to.toString());
	               Log.d("rate", rate.toString());
	               Log.d("from", from.toString());
*/
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
              

               try {
   				Thread.sleep(3000);
   			} catch (InterruptedException e) {
   				// TODO Auto-generated catch block
   				e.printStackTrace();
   			}
                  
              return "Executed";
          }

          @Override
          protected void onPostExecute(String result) {
        	  progress.dismiss();
				
              // might want to change "executed" for the returned string passed
              // into onPostExecute() but that is upto you
          }

          @Override
          protected void onPreExecute() {  progress.show();
			}

          @Override
          protected void onProgressUpdate(Void... values) {}
      }
  
      @Override
      public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.main, menu);
            return true;
      }
 
      public void SelectItem(int possition) {
            Fragment fragment = null;
            Bundle args = new Bundle();
            switch (possition) {
            case 0:
            	fragment = new SelectCountries();
            	position = 0;
                  break;
            case 1:
                  fragment = new Summary();
              	position = 1;
                  break;
            case 2:
                  fragment = new Currency();
              	position = 2;
                  break;
            case 3:
                  fragment = new Temperature();
              	position = 3;
                  break;
            case 4:
                  fragment = new Languages();
              	position = 4;
                  break;
            case 5:
                  fragment = new PlugTypes();
              	position = 5;
                  break;
            case 6:
                  fragment = new Measurements();
              	position = 6;
                  break;
            case 7: 
                fragment = new DateTime();
            /*    Bundle bundle = new Bundle();
                bundle.putString("origincountry", MainActivity.getOrigin().toString());
                bundle.putString("destinationcountry", MainActivity.getDestination().toString());
                fragment.setArguments(bundle);
            */	position = 7;
                break;
          
            	//Toast.makeText(MainActivity.this,"Refresh clicked", 5000).show();
            /* Unitl Later 
            case 7:
                  fragment = new Customize();
                  break;
            case 8:
                  fragment = new Admin();
                  break;
                  */
            default:
                  break;
            }
        //    if(position!=7)
         //   {
            fragment.setArguments(args);
       //     }
            FragmentManager frgManager = getFragmentManager();
            frgManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
 
            mDrawerList.setItemChecked(possition, true);
            setTitle(dataList.get(possition).getItemName());
            mDrawerLayout.closeDrawer(mDrawerList);
 
      }
 
      @Override
      public void setTitle(CharSequence title) {
            mTitle = title;
            getActionBar().setTitle(mTitle);
      }
 
      @Override
      protected void onPostCreate(Bundle savedInstanceState) {
            super.onPostCreate(savedInstanceState);
            // Sync the toggle state after onRestoreInstanceState has occurred.
            mDrawerToggle.syncState();
      }
 
      @Override
      public void onConfigurationChanged(Configuration newConfig) {
            super.onConfigurationChanged(newConfig);
            // Pass any configuration change to the drawer toggles
            mDrawerToggle.onConfigurationChanged(newConfig);
      }
 
      @Override
      public boolean onOptionsItemSelected(MenuItem item) {
            // The action bar home/up action should open or close the drawer.
            // ActionBarDrawerToggle will take care of this.
            if (mDrawerToggle.onOptionsItemSelected(item)) {
                  return true;
            }else if(item.getItemId() == R.id.help){
            	FragmentManager frgManager = getFragmentManager();
            	Fragment fragment = null;
            	fragment = new Help();
                frgManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
            }
            return false;
      }
      private static boolean isConnected(Context context) {
    	    ConnectivityManager connectivityManager = (ConnectivityManager)
    	        context.getSystemService(Context.CONNECTIVITY_SERVICE);
    	    NetworkInfo networkInfo = null;
    	    if (connectivityManager != null) {
    	        networkInfo =
    	            connectivityManager.getActiveNetworkInfo();
    	        if(networkInfo.isConnected())
    	        {
    	        //	Toast.makeText(context,"isconnected", 5000).show();
    	        	//networkInfo.isConnected();
    	        }
    	    }
    	    return networkInfo == null ? false : true;
    	}
        private class DrawerItemClickListener implements ListView.OnItemClickListener {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
            	if(position == 8){
            		//Toast.makeText(MainActivity.this, "position=8",5000).show();
            		boolean check = isConnected(MainActivity.this);     		
            		if(check){
					progress = ProgressDialog.show(MainActivity.this, "Fetching Results..",
					  "Fetching Currency Rates", true);
						
					new Thread(new Runnable() {
					  @Override
					  public void run()
					  { 
						  for(int i=0;i<=countriesName.length-1;i++)
						  {
							
						    new LongOperation().execute(String.valueOf(i));
						  
						  }
						 
					    /*runOnUiThread(new Runnable() {
					      @Override
					      public void run()
					      {
					    	  progress.show();    	
					      }
					    });*/
					  }
					}).start();
					
            		/*try {
						Thread.sleep(5000);
						progress.dismiss();
						 // progress.dismiss();
            		} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
			//		progress.dismiss();
					
            		}else
            		{
            		Toast.makeText(MainActivity.this,"Please turn on your wifi then try again !!", 7000).show();	
            		}
   //SelectItem(position);
            	//This seletcItem selct the position
            	}else if (position == 7){
            //	Toast.makeText(MainActivity.this, "position=7 DAte time clicked",5000).show();
            		
            	SelectItem(position);
            	}else if (origin == null){
            		SelectItem(0);
            	}else{
            		SelectItem(position);
            	}
            }
      }

        public static String getOrigin(){
			return origin;
        }
        
        public void setOriginposition(int origin){
        	this.originposition = origin;
        }
        public static int getOriginpostion(){
			return originposition;
        }
        
        public void setOrigin(String origin){
        	this.origin = origin;
        }

        public static String getDestination(){
    			return destination;
            }
            
        public void setDestination(String destination){
            	this.destination = destination;
            }

        public static int getDestinationposition(){
    			return destinationposition;
            }
            
        public void setDestinationposition(int d){
            	this.destinationposition = d;
            }
 
}