package com.example.travelcompanion;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.fitscholar.travelcompanion.R;

/* This class allows the user to select both the origin and the destination country.  Upon the enter button the user
 * is sent to the summary page.  The class sets both the origin and destination country selected from the spinner
 * menus that are taken globally from the database in 'strings' and sets them globally in MainActivity.   */ 

public class SelectCountries extends Fragment {
	 
	//the following are viewtype items 
    TextView label1,label2, display;
    TextView select_countries_instruction;
    ImageView wtc_logo;
    Spinner origin;
    Spinner destination;
    Button button;
    String originText;
    String destinationText;
   int originposition= 0;
   int destinationposition= 0;


    public SelectCountries() {

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
        final MainActivity activity = (MainActivity) getActivity();

          View view = inflater.inflate(R.layout.select_countries_layout, container,
                      false);
          
          //the following makes all viewtype variables visable in the feild 
          origin = (Spinner) view.findViewById(R.id.select_countries_origin);
          destination = (Spinner) view.findViewById(R.id.select_countries_destination);
          button = (Button) view.findViewById(R.id.select_countries_button);
          button.setOnClickListener(new View.OnClickListener() {
        	  //this button gets the selected countries from the spinners, sets them globally in MainActivity, and sends to Summary Page
        	  public void onClick(View v) {
        	    	 originText = origin.getSelectedItem().toString();
        	          destinationText = destination.getSelectedItem().toString();
        	            originposition = origin.getSelectedItemPosition();
        	           destinationposition = destination.getSelectedItemPosition();
         	          
        	           if(originText.matches("Choose Country") || destinationText.matches("Choose Country")){
        	        	  Context context = getActivity().getApplicationContext();
        	        	  CharSequence text = "Please Choose Both Countries!";
        	        	  int duration = Toast.LENGTH_SHORT;

        	        	  Toast toast = Toast.makeText(context, text, duration);
        	        	  toast.show();
        	          }else{
        	         activity.setOrigin(originText);
        	         activity.setDestination(destinationText);
        	         activity.setOriginposition(originposition);
        	         activity.setDestinationposition(destinationposition);
        	         
        	         activity.SelectItem(1); 
        	          }
        	    }
        	});
          wtc_logo = (ImageView) view.findViewById(R.id.wtc_logo);
          label1 = (TextView) view.findViewById(R.id.select_countries_label1);
          label2 = (TextView) view.findViewById(R.id.select_countries_label2);
          display = (TextView) view.findViewById(R.id.selected_countries_display);
          select_countries_instruction = (TextView) view.findViewById(R.id.select_countries_instruction);
          
          //this code sets viewable text views to desired output
          wtc_logo.setImageResource(R.drawable.wtc_logo);
          if(activity.getOrigin() == null){
        	  display.setText("Choose Country  ->  Choose Country");
          }
          else{
          display.setText(activity.getOrigin() + " -> " + activity.getDestination());
          }
          
          return view;
          
    }
    

}
