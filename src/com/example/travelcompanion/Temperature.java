package com.example.travelcompanion;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.fitscholar.travelcompanion.R;

/* This class is a calculator that allows the user to put in a temperature for either the origin or destination country and then 
 * see the equivelent of the respective other countries temp.  */ 

public class Temperature extends Fragment {
	//the following are viewtype items 
    TextView temperature_origin;
    EditText temperature_origin_number;
    TextView temperature_origin_label;
    TextView temperature_destination;
    EditText temperature_destination_number;
    TextView temperature_destination_label;
    Button button;
    Button clear;
    TextView temperature_instructions;
    TextView temperature_display;



    public Temperature() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {

          View view = inflater.inflate(R.layout.temperature_layout, container,
                      false);
          
          DatabaseHelper dbHelper = new DatabaseHelper(getActivity());
          final String originTemperature= dbHelper.getOriginTemperature();
          final String destinationTemperature = dbHelper.getDestinationTemperature();
          
        //the following makes all viewtype variables visable in the feild
          temperature_origin = (TextView) view.findViewById(R.id.temperature_origin);
          temperature_origin_number = (EditText) view.findViewById(R.id.temperature_origin_number);
          temperature_origin_number.setOnFocusChangeListener(new OnFocusChangeListener() {
        	  @Override
        	  public void onFocusChange(View v, boolean hasFocus) {
        		  if(hasFocus){
        			  temperature_origin_number.setText("");
        			  temperature_destination_number.setText("");
        		  }else {
        			  temperature_origin_number.setText("");
        			  temperature_destination_number.setText("");
        		  }
        	  }
          });
          temperature_origin_label = (TextView) view.findViewById(R.id.temperature_origin_label);
          temperature_destination = (TextView) view.findViewById(R.id.temperature_destination);
          temperature_destination_number = (EditText) view.findViewById(R.id.temperature_destination_number);
          temperature_destination_number.setOnFocusChangeListener(new OnFocusChangeListener() {
        	  @Override
        	  public void onFocusChange(View v, boolean hasFocus) {
        		  if(hasFocus){
        			  temperature_destination_number.setText("");
        			  temperature_origin_number.setText("");
        		  }else {
        			  temperature_origin_number.setText("");
        			  temperature_destination_number.setText("");
        		  }
        	  }
          });
          temperature_destination_label = (TextView) view.findViewById(R.id.temperature_destination_label);
          button = (Button) view.findViewById(R.id.temperature_go);
          button.setOnClickListener(new View.OnClickListener() {
      	    public void onClick(View v) {
      	    	String originTemp = temperature_origin_number.getText().toString();
    	    	String destinationTemp = temperature_destination_number.getText().toString();
    	    	if(originTemp.matches("") && !destinationTemp.matches("")){
    	    		if (destinationTemperature.matches("Fahrenheit") && originTemperature.matches("Fahrenheit")){
    	    			temperature_origin_number.setText(String.valueOf(temperature_destination_number.getText()));
    	    		}
    	    		else if (destinationTemperature.matches("Fahrenheit") && originTemperature.matches("Celsius")){
    	    			temperature_origin_number.setText(String.valueOf((Double.parseDouble(temperature_destination_number.getText().toString())-32)*(.5556)));
    	    		}
    	    		else if (destinationTemperature.matches("Celsius") && originTemperature.matches("Fahrenheit")){
    	    			temperature_origin_number.setText(String.valueOf((Double.parseDouble(destinationTemp))*(1.8)+32));
    	    		}
    	    		else if (destinationTemperature.matches("Celsius") && originTemperature.matches("Celsius")){
    	    			temperature_origin_number.setText(String.valueOf(temperature_destination_number.getText()));
    	    		}
    	    	}
    	    	else if (destinationTemp.matches("") && !originTemp.matches("")){
    	    		if (destinationTemperature.matches("Fahrenheit") && originTemperature.matches("Fahrenheit")){
    	    			temperature_destination_number.setText(String.valueOf(temperature_origin_number.getText()));
    	    		}
    	    		else if (destinationTemperature.matches("Fahrenheit") && originTemperature.matches("Celsius")){
    	    			temperature_destination_number.setText(String.valueOf(Double.parseDouble(temperature_origin_number.getText().toString())*(1.8)+32));
    	    		}
    	    		else if (destinationTemperature.matches("Celsius") && originTemperature.matches("Fahrenheit")){
    	    			temperature_destination_number.setText(String.valueOf((Double.parseDouble(temperature_origin_number.getText().toString())-32)*(.5556)));
    	    		}
    	    		else if (destinationTemperature.matches("Celsius") && originTemperature.matches("Celsius")){
    	    			temperature_destination_number.setText(String.valueOf(temperature_origin_number.getText()));
    	    		}
    	    	}else{
    	    		Context context = getActivity().getApplicationContext();
  	        	  CharSequence text = "Please Enter Temperature!";
  	        	  int duration = Toast.LENGTH_SHORT;

  	        	  Toast toast = Toast.makeText(context, text, duration);
  	        	  toast.show();
    	    	}
      	    	
      	    }
      	});
          clear = (Button) view.findViewById(R.id.temperature_clear);
          clear.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				temperature_origin_number.setText("");
				temperature_destination_number.setText("");
			}
		});
          temperature_instructions = (TextView) view.findViewById(R.id.temperature_instructions);
          temperature_display = (TextView) view.findViewById(R.id.temperature_display);
          LinearLayout rlayout = (LinearLayout) view.findViewById(R.id.temperature_layout);
          rlayout.setOnClickListener(new View.OnClickListener() {
        	  @Override
        	  public void onClick(View v){
        		InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
      	    		imm.hideSoftInputFromWindow(temperature_origin_number.getWindowToken(), 0);
        	  }
          });
          
          
          //the following code gets the origin and destination from this fragments activity, MainActivity
          MainActivity activity = (MainActivity) getActivity();
          String origin = activity.getOrigin();
          String destination = activity.getDestination();
          
          //this code sets viewable text views to desired output
          temperature_origin.setText(origin);
          temperature_destination.setText(destination);
  
        //the following code gets information from DatabaseHelper
          String originLabel = dbHelper.getOriginTemperature();
          String destinationLabel = dbHelper.getDestinationTemperature();
          
          temperature_origin_label.setText(originLabel);
          temperature_destination_label.setText(destinationLabel);
          
          temperature_display.setText(origin + " - > " + destination);
          
          return view;
    }

}
