package com.example.travelcompanion;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.fitscholar.travelcompanion.R;

/* This class is a display class that shows common languages for both the origin and destination country  */ 

public class Help extends Fragment {
	//the following are viewtype items  
	TextView help_select;
	TextView help_summary;
	TextView help_currency;
	TextView help_temp;
	TextView help_languages;
	TextView help_plug;
	TextView help_measure;
	ImageView select;
	ImageView summary;
	ImageView currency;
	ImageView temp;
	ImageView languages;
	ImageView plug;
	ImageView measure;




    public Help() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {

    	final MainActivity activity = (MainActivity) getActivity();
    	
          View view = inflater.inflate(R.layout.help_layout, container,
                      false);

          //the following makes all viewtype variables visable in the feild 
          help_select = (TextView) view.findViewById(R.id.help_select);
          help_select.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        	      if(activity.getOrigin() == null){
        	    	  activity.SelectItem(0);
        	      }else
        		  activity.SelectItem(0); 
        	          }
        	});
          help_summary = (TextView) view.findViewById(R.id.help_summary);
          help_summary.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  if(activity.getOrigin() == null){
        	    	  activity.SelectItem(0);
        	    	  Context context = getActivity().getApplicationContext();
    	        	  CharSequence text = "Select Countries Before Viewing SummaryPage";
    	        	  int duration = Toast.LENGTH_SHORT;

    	        	  Toast toast = Toast.makeText(context, text, duration);
    	        	  toast.show();
        	      }else
        		  activity.SelectItem(1); 
        	          }
        	});
          help_currency = (TextView) view.findViewById(R.id.help_currency);
          help_currency.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  if(activity.getOrigin() == null){
        	    	  activity.SelectItem(0);
        	    	  Context context = getActivity().getApplicationContext();
    	        	  CharSequence text = "Select Countries Before Viewing CurrencyCalculator";
    	        	  int duration = Toast.LENGTH_SHORT;

    	        	  Toast toast = Toast.makeText(context, text, duration);
    	        	  toast.show();
        	      }else
        		  activity.SelectItem(2); 
        	          }
        	});
          help_temp = (TextView) view.findViewById(R.id.help_temp);
          help_temp.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  if(activity.getOrigin() == null){
        	    	  activity.SelectItem(0);
        	    	  Context context = getActivity().getApplicationContext();
    	        	  CharSequence text = "Select Countries Before Viewing TemperatureCalculator";
    	        	  int duration = Toast.LENGTH_SHORT;

    	        	  Toast toast = Toast.makeText(context, text, duration);
    	        	  toast.show();
        	      }else
        		  activity.SelectItem(3); 
        	          }
        	});
          help_languages = (TextView) view.findViewById(R.id.help_languages);
          help_languages.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  if(activity.getOrigin() == null){
        	    	  activity.SelectItem(0);
        	    	  Context context = getActivity().getApplicationContext();
    	        	  CharSequence text = "Select Countries Before Viewing LanguagesPage";
    	        	  int duration = Toast.LENGTH_SHORT;

    	        	  Toast toast = Toast.makeText(context, text, duration);
    	        	  toast.show();
        	      }else
        		  activity.SelectItem(4); 
        	          }
        	});
          help_plug = (TextView) view.findViewById(R.id.help_plug);
          help_plug.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  if(activity.getOrigin() == null){
        	    	  activity.SelectItem(0);
        	    	  Context context = getActivity().getApplicationContext();
    	        	  CharSequence text = "Select Countries Before Viewing PlugTypesPage";
    	        	  int duration = Toast.LENGTH_SHORT;

    	        	  Toast toast = Toast.makeText(context, text, duration);
    	        	  toast.show();
        	      }else
        		  activity.SelectItem(5); 
        	          }
        	});
          help_measure = (TextView) view.findViewById(R.id.help_measure);
          help_measure.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  if(activity.getOrigin() == null){
        	    	  activity.SelectItem(0);
        	    	  Context context = getActivity().getApplicationContext();
    	        	  CharSequence text = "Select Countries Before Viewing MeasurementsPage";
    	        	  int duration = Toast.LENGTH_SHORT;

    	        	  Toast toast = Toast.makeText(context, text, duration);
    	        	  toast.show();
        	      }else
        		  activity.SelectItem(6); 
        	          }
        	});
          select = (ImageView) view.findViewById(R.id.select);
          select.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  if(activity.getOrigin() == null){
        	    	  activity.SelectItem(0);
        	      }else
        		  activity.SelectItem(0); 
        	          }
        	});
          summary = (ImageView) view.findViewById(R.id.summary);
          summary.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  if(activity.getOrigin() == null){
        	    	  activity.SelectItem(0);
        	    	  Context context = getActivity().getApplicationContext();
    	        	  CharSequence text = "Select Countries Before Viewing SummaryPage";
    	        	  int duration = Toast.LENGTH_SHORT;

    	        	  Toast toast = Toast.makeText(context, text, duration);
    	        	  toast.show();
        	      }else
        		  activity.SelectItem(1); 
        	          }
        	});
          currency = (ImageView) view.findViewById(R.id.currency);
          currency.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  if(activity.getOrigin() == null){
        	    	  activity.SelectItem(0);
        	    	  Context context = getActivity().getApplicationContext();
    	        	  CharSequence text = "Select Countries Before Viewing CurrencyCalculator";
    	        	  int duration = Toast.LENGTH_SHORT;

    	        	  Toast toast = Toast.makeText(context, text, duration);
    	        	  toast.show();
        	      }else
        		  activity.SelectItem(2); 
        	          }
        	});
          temp = (ImageView) view.findViewById(R.id.temp);
          temp.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  if(activity.getOrigin() == null){
        	    	  activity.SelectItem(0);
        	    	  Context context = getActivity().getApplicationContext();
    	        	  CharSequence text = "Select Countries Before Viewing TemperatureCalculator";
    	        	  int duration = Toast.LENGTH_SHORT;

    	        	  Toast toast = Toast.makeText(context, text, duration);
    	        	  toast.show();
        	      }else
        		  activity.SelectItem(3); 
        	          }
        	});
          languages = (ImageView) view.findViewById(R.id.languages);
          languages.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  if(activity.getOrigin() == null){
        	    	  activity.SelectItem(0);
        	    	  Context context = getActivity().getApplicationContext();
    	        	  CharSequence text = "Select Countries Before Viewing LanguagesPage";
    	        	  int duration = Toast.LENGTH_SHORT;

    	        	  Toast toast = Toast.makeText(context, text, duration);
    	        	  toast.show();
        	      }else
        		  activity.SelectItem(4); 
        	          }
        	});
          plug = (ImageView) view.findViewById(R.id.plug);
          plug.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  if(activity.getOrigin() == null){
        	    	  activity.SelectItem(0);
        	    	  Context context = getActivity().getApplicationContext();
    	        	  CharSequence text = "Select Countries Before Viewing PlugTypesPage";
    	        	  int duration = Toast.LENGTH_SHORT;

    	        	  Toast toast = Toast.makeText(context, text, duration);
    	        	  toast.show();
        	      }else
        		  activity.SelectItem(5); 
        	          }
        	});
          measure = (ImageView) view.findViewById(R.id.measure);
          measure.setOnClickListener(new View.OnClickListener() {  
        	  public void onClick(View v) {
        		  if(activity.getOrigin() == null){
        	    	  activity.SelectItem(0);
        	    	  Context context = getActivity().getApplicationContext();
    	        	  CharSequence text = "Select Countries Before Viewing MeasurementsPage";
    	        	  int duration = Toast.LENGTH_SHORT;

    	        	  Toast toast = Toast.makeText(context, text, duration);
    	        	  toast.show();
        	      }else
        		  activity.SelectItem(6); 
        	          }
        	});


          //the following code gets the origin and destination from this fragments activity, MainActivity

          
          
          //this code sets viewable text views to desired output

          
          return view;
    }

}