package com.example.travelcompanion;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.travelcompanion.utility.AllEntries;

/*
 This class implements a database that has the following structure:  it also ques statements to be retreived from other classes.
 
                                        CountriesTable
---------------------------------------------------------------------------------------------------------------------------------------------------
| Country | CurrencyType | CurrencyConversion  | Temperature | Languages | PlugType | PlugTypeVolt | PlugTypeFreq | Measurements | Common Measure |
---------------------------------------------------------------------------------------------------------------------------------------------------
| Data    |              |                     |             |           |          |              |              |              |                |      
---------------------------------------------------------------------------------------------------------------------------------------------------
|   "     |              |                     |             |           |          |              |              |              |                |      
---------------------------------------------------------------------------------------------------------------------------------------------------          
 */

public class DatabaseHelper extends SQLiteOpenHelper {

	String n1ton2value="";
	String n2ton1value="";
	static String labels = "" ;
	//code names database information and columns 
	static final String dbName = "countriesDB";
	static final String countriesTable = "CountriesTable";
	static final String colID = "Country";
	static final String colCurrencyType = "CurrencyType";
	static final String CurrencyConversiontoUSD = "CurrencyConversiontoUSD";//baad main ha
	static final String CurrencyConversionfromUSD = "CurrencyConversionfromUSD";//phly ha
	static final String colTemperature = "Temperature";
	static final String colLanguages = "Languages";
	static final String colPlugType = "PlugType";
	static final String colPlugTypeVolt = "PlugTypeVolt";
	static final String colPlugTypeFreq = "PlugTypeFreq";
	static final String colMeasurements = "Measurements";
	static final String colCommonMeasure = "CommonMeasure";
	
	String originCountry = MainActivity.getOrigin();
	String destinationCountry = MainActivity.getDestination();

	private final String allcountriesTable="allcountriesDB";
	private final String countryID="countryID";
	private final String countryName="countryName";
	/** Entries table   **/
	
	private final String entriesTable="allentriesDB";
	private final String entries_countryID="entries_countryID";
	private final String entries_countryName="entries_countryName";
	private final String entries_countryValue="entries_countryValue";
	private final String entries_multipletime="entries_mulipletimezone";
	private final String entries_string="entries_string";
	private final String entries_plus="entries_plus";


	private final String allmetricsTable="allmetricDB";
	private final String metricID="metricID";
	private final String metricName="metricName";
	
	private final String metricTable = "metricDB";
	private final String metricoriginalID = "metricoriginalID";
	private final String metricName1toName2 = "name1to2";
	private final String metricName1toName2value = "name1to2value";
	private final String metricName2toName1 = "name2to1";
	private final String metricName2toName1value = "name2to1value";
	
	
	public int counter=0;
	
	//this constructor keeps track of the most current database.
	//for example database 1 was a test for just one column, thus 34 represents the most current database for the onUpdate method
	public DatabaseHelper(Context context)
	{
		super(context, dbName, null, 50);
	}

//this method creates the Database
public void onCreate(SQLiteDatabase db){
	db.execSQL("CREATE TABLE "+countriesTable + "("+colID+" TEXT PRIMARY KEY , "+
		        colCurrencyType+" TEXT, "+CurrencyConversionfromUSD+" DOUBLE, "+CurrencyConversiontoUSD+" DOUBLE, "+colTemperature+ 
		    " TEXT, "+colLanguages+" TEXT, "+colPlugType+" TEXT, "+colPlugTypeVolt+" INTEGER, "+ 
		    colPlugTypeFreq+" INTEGER, "+colMeasurements+" TEXT, "+colCommonMeasure+" TEXT);");
	db.execSQL("CREATE TABLE "+ allcountriesTable + "("+countryID+" integer PRIMARY KEY , "+
	        countryName+" TEXT );");
	db.execSQL("CREATE TABLE "+ metricTable + "("+metricoriginalID+" integer PRIMARY KEY , "+
	        metricName1toName2+" TEXT,"+
	        metricName1toName2value+" TEXT," +
	        metricName2toName1+" TEXT,"+
	        metricName2toName1value+" TEXT"+
	        ");");
	db.execSQL("CREATE TABLE "+ allmetricsTable + "("+metricID+" integer PRIMARY KEY , "+
	        metricName+" TEXT );");
	db.execSQL("CREATE TABLE "+ entriesTable + "("+
	        entries_countryID + " INTEGER PRIMARY KEY, "+
			entries_countryName +" TEXT," +
			entries_countryValue +" TEXT,"+ 
			entries_multipletime + " TEXT,"+ 
	        entries_string + " TEXT," + entries_plus + " TEXT" + ");");
	
	
	String Italy = "INSERT INTO " + countriesTable + " Values ('Italy','Euro',0.72,1.36,'Celsius','Italian, German, French, Slovene','C,F,L', 230, 50,'Italian Metric','2.5 Centimeters,.3 Meters,1.6 Kilometers,28.3 Grams,.45 Kilograms,29.6 Milliliters,3.8 Liters,16.1 Kilometers Per Hour');";
	db.execSQL(Italy);
	
	String UnitedStates = "INSERT INTO " + countriesTable + " Values ('United States','U.S. Dollar',1,1,'Fahrenheit','English, Spanish','A,B', 120, 60,'U.S. (Standard)','1 Inch,1 Foot,1 Mile,1 Ounce,1 Pound,8 Fluid Ounces,1 Gallon,10 Miles Per Hour');";
	db.execSQL(UnitedStates);
	
	String Mexico = "INSERT INTO " + countriesTable + " Values ('Mexico','Peso',13.09,0.0777,'Celsius','Spanish, various Mayan, Nahuatl, and other regional indigenous languages.','A,B', 127, 60,'Metric','2.5 Centimeters,.3 Meters,1.6 Kilometers,28.3 Grams,.45 Kilograms,29.6 Milliliters,3.8 Liters,16.1 Kilometers Per Hour');";
	db.execSQL(Mexico);

	String Canada = "INSERT INTO " + countriesTable + " Values ('Canada','Canadian Dollar',1.10,0.92,'Celsius','English, French, 53 native Inuit and American-Indian languages','A,B', 120, 60,'Metric','2.5 Centimeters,.3 Meters,1.6 Kilometers,28.3 Grams,.45 Kilograms,29.6 Milliliters,3.8 Liters,16.1 Kilometers Per Hour');";
	db.execSQL(Canada);

	String China = "INSERT INTO " + countriesTable + " Values ('China','Yuan',6.25,0.16,'Celsius','Mandarin, Hanyu, Cantonese, Shanghainese','A,I,G', 220, 50,'Metric','2.5 Centimeters,.3 Meters,1.6 Kilometers,28.3 Grams,.45 Kilograms,29.6 Milliliters,3.8 Liters,16.1 Kilometers Per Hour');";
	db.execSQL(China);

	String Japan = "INSERT INTO " + countriesTable + " Values ('Japan','Yen',102.57,0.009810,'Celsius','Japanese, Ryukyuan','A,B', 100, 50,'Metric & U.S. (Standard)','2.5 Centimeters,.3 Meters,1.6 Kilometers,28.3 Grams,.45 Kilograms,29.6 Milliliters,3.8 Liters,16.1 Kilometers Per Hour');";
	db.execSQL(Japan);

	String Singapore = "INSERT INTO " + countriesTable + " Values ('Singapore','Brunei Dollar',1.26,0.798,'Celsius','English, Malay, Mandarin, Cantonese','G', 230, 50,'Metric','2.5 Centimeters,.3 Meters,1.6 Kilometers,28.3 Grams,.45 Kilograms,29.6 Milliliters,3.8 Liters,16.1 Kilometers Per Hour');";
	db.execSQL(Singapore);

	String Aruba = "INSERT INTO " + countriesTable + " Values ('Aruba','Florin',1.79,0.558,'Celsius','Dutch, Papiamento (Creole with Spanish, Portuguese, Dutch, English roots), English (widely spoken), Spanish','A,B,F', 127, 60,'Metric','2.5 Centimeters,.3 Meters,1.6 Kilometers,28.3 Grams,.45 Kilograms,29.6 Milliliters,3.8 Liters,16.1 Kilometers Per Hour');";
	db.execSQL(Aruba);

	String NewZealand = "INSERT INTO " + countriesTable + " Values ('New Zealand','New Zealand Dollar',1.17,0.8548,'Celsius','English, Maori','I,', 230, 50,'Metric','2.5 Centimeters,.3 Meters,1.6 Kilometers,28.3 Grams,.45 Kilograms,29.6 Milliliters,3.8 Liters,16.1 Kilometers Per Hour');";
	db.execSQL(NewZealand);

	String Australia = "INSERT INTO " + countriesTable + " Values ('Australia','Australian Dollar',1.08,0.9228,'Celsius','English, Chinese, Italian, Greek','I', 240, 50,'Metric','2.5 Centimeters,.3 Meters,1.6 Kilometers,28.3 Grams,.45 Kilograms,29.6 Milliliters,3.8 Liters,16.1 Kilometers Per Hour');";
	db.execSQL(Australia);

	String France = "INSERT INTO " + countriesTable + " Values ('France','Euro',.72,1.3633,'Celsius','French, Provencal, Breton, Alsatian, Corsican, Catalan','E', 230, 50,'Metric','2.5 Centimeters,.3 Meters,1.6 Kilometers,28.3 Grams,.45 Kilograms,29.6 Milliliters,3.8 Liters,16.1 Kilometers Per Hour');";
	db.execSQL(France);

	String GreatBritain = "INSERT INTO " + countriesTable + " Values ('Great Britain','British Pound',.59,1.6834,'Celsius','English, Welsh, Scottish form of Gaelic','G', 230, 50,'Metric & U.S. (Standard)','2.5 Centimeters,.3 Meters,1.6 Kilometers,28.3 Grams,.45 Kilograms,29.6 Milliliters,3.8 Liters,16.1 Kilometers Per Hour');";
	db.execSQL(GreatBritain);

	String country1 = "INSERT INTO " + allcountriesTable + " Values (1,'Aruba');";
	String country2 = "INSERT INTO " + allcountriesTable + " Values (2,'Australia');";
	String country3 = "INSERT INTO " + allcountriesTable + " Values (3,'Canada');";
	String country4 = "INSERT INTO " + allcountriesTable + " Values (4,'China');";
	String country5 = "INSERT INTO " + allcountriesTable + " Values (5,'France');";
	String country6 = "INSERT INTO " + allcountriesTable + " Values (6,'Great Britian');";
	String country7 = "INSERT INTO " + allcountriesTable + " Values (7,'Italy');";
	String country8 = "INSERT INTO " + allcountriesTable + " Values (8,'Japan');";
	String country9 = "INSERT INTO " + allcountriesTable + " Values (9,'Mexico');";
	String country10 = "INSERT INTO " + allcountriesTable + " Values (10,'New Zealand');";
	String country11 = "INSERT INTO " + allcountriesTable + " Values (11,'Singapore');";
	String country12 = "INSERT INTO " + allcountriesTable + " Values (12,'America');";

	db.execSQL(country1);
	db.execSQL(country2);
	db.execSQL(country3);
	db.execSQL(country4);
	db.execSQL(country5);
	db.execSQL(country6);
	db.execSQL(country7);
	db.execSQL(country8);
	db.execSQL(country9);
	db.execSQL(country10);
	db.execSQL(country11);
	db.execSQL(country12);
	
	
	

	String aruba1 = "INSERT INTO " + entriesTable + " Values (1,'Aruba','Oranjestad','No','America/Aruba','0');";//Aruba
	db.execSQL(aruba1);
	
	String australia1 = "INSERT INTO " + entriesTable + " Values (2,'Australia','Sydney','Yes','Australia/Sydney','0');";
	String australia2 = "INSERT INTO " + entriesTable + " Values (3,'Australia','Melbourne','Yes','Australia/Melbourne','0');";
	String australia3 = "INSERT INTO " + entriesTable + " Values (4,'Australia','Perth','Yes','Australia/Perth','0');";
	String australia4 = "INSERT INTO " + entriesTable + " Values (5,'Australia','Tasmania','Yes','Australia/Tasmania','0');";
	String australia5 = "INSERT INTO " + entriesTable + " Values (6,'Australia','Victoria','Yes','Australia/Victoria','0');";
	String australia6 = "INSERT INTO " + entriesTable + " Values (7,'Australia','Queensland','Yes','Australia/Queensland','0');";
	String australia7 = "INSERT INTO " + entriesTable + " Values (8,'Australia','Canberra','Yes','Australia/Canberra','0');";
	String australia8 = "INSERT INTO " + entriesTable + " Values (9,'Australia','Brisbane','Yes','Australia/Brisbane','0');";
	String australia9 = "INSERT INTO " + entriesTable + " Values (10,'Australia','Lindeman','Yes','Australia/Lindeman','0');";
	String australia10 = "INSERT INTO " + entriesTable + " Values (11,'Australia','Adelaide','Yes','Australia/Adelaide','0');";
	db.execSQL(australia1);
	db.execSQL(australia2);
	db.execSQL(australia3);
	db.execSQL(australia4);
	db.execSQL(australia5);
	db.execSQL(australia6);
	db.execSQL(australia7);
	db.execSQL(australia8);
	db.execSQL(australia9);
	db.execSQL(australia10);
	
	String canada1 = "INSERT INTO " + entriesTable + " Values (12,'Canada','Winnipeg','Yes','America/Winnipeg','3600000');";
	String canada2 = "INSERT INTO " + entriesTable + " Values (13,'Canada','Regina','Yes','America/Regina','0');";
	String canada3 = "INSERT INTO " + entriesTable + " Values (14,'Canada','Yukon','Yes','Canada/Yukon','0');";
	String canada4 = "INSERT INTO " + entriesTable + " Values (15,'Canada','Edmonton','Yes','America/Edmonton','3600000');";
	String canada5 = "INSERT INTO " + entriesTable + " Values (16,'Canada','Vancouver','Yes','America/Vancouver','3600000');";
	db.execSQL(canada1);
	db.execSQL(canada2);
	db.execSQL(canada3);
	db.execSQL(canada4);
	db.execSQL(canada5);
	
	String china1 = "INSERT INTO " + entriesTable + " Values (17,'China','Shanghai','No','Asia/Shanghai','0');";
	String china2 = "INSERT INTO " + entriesTable + " Values (18,'China','Chongqing','No','Asia/Chongqing','0');";
	db.execSQL(china1);
	db.execSQL(china2);
	
	String france1 = "INSERT INTO " + entriesTable + " Values (19,'France','Paris','No','Europe/Paris','0');";
	db.execSQL(france1);
	
	String greatbritian1 = "INSERT INTO " + entriesTable + " Values (20,'Great Britain','London','No','Europe/London','0');";
	db.execSQL(greatbritian1);
	
	String italy1 = "INSERT INTO " + entriesTable + " Values (21,'Italy','Rome','No','Europe/Rome','0');";
	db.execSQL(italy1);
	
	String japan1 = "INSERT INTO " + entriesTable + " Values (22,'Japan','Tokyo','No','Asia/Tokyo','0');";
	db.execSQL(japan1);

	String mexico1 = "INSERT INTO " + entriesTable + " Values (23,'Mexico','Tijuana','Yes','America/Tijuana','3600000');";
	String mexico2 = "INSERT INTO " + entriesTable + " Values (24,'Mexico','Mexico City','Yes','America/Mexico_City','3600000');";
	String mexico3 = "INSERT INTO " + entriesTable + " Values (241,'Mexico','BajaSur','Yes','Mexico/BajaSur','3600000');";
	
	db.execSQL(mexico1);
	db.execSQL(mexico2);
	db.execSQL(mexico3);
	
	String newzealand1 = "INSERT INTO " + entriesTable + " Values (25,'New Zealand','Auckland','Yes','Pacific/Auckland','0');";
	String newzealand2 = "INSERT INTO " + entriesTable + " Values (26,'New Zealand','Chatham Island','Yes','Pacific/Chatham','0');";
	db.execSQL(newzealand1);
	db.execSQL(newzealand2);
	
	String singapore1 = "INSERT INTO " + entriesTable + " Values (27,'Singapore','Singapore City','No','Asia/Singapore','0');";
	db.execSQL(singapore1);
	
	String america1 = "INSERT INTO " + entriesTable + " Values (28,'United States','Chicago','Yes','America/Chicago','3600000');";
	String america2 = "INSERT INTO " + entriesTable + " Values (29,'United States','Denver','Yes','America/Denver','3600000');";
	String america3 = "INSERT INTO " + entriesTable + " Values (30,'United States','Phoenix','Yes','America/Phoenix','0');";
	String america4 = "INSERT INTO " + entriesTable + " Values (31,'United States','Los Angeles','Yes','America/Los_Angeles','0');";
	String america5 = "INSERT INTO " + entriesTable + " Values (32,'United States','Anchorage','Yes','America/Anchorage','3600000');";
	String america6 = "INSERT INTO " + entriesTable + " Values (33,'United States','Honolulu','Yes','Pacific/Honolulu','0');";
	String america7 = "INSERT INTO " + entriesTable + " Values (34,'United States','Indianapolis','Yes','America/Indianapolis','3600000');";
	String america8 = "INSERT INTO " + entriesTable + " Values (35,'United States','Detroit','Yes','America/Detroit','3600000');";
	String america9 = "INSERT INTO " + entriesTable + " Values (36,'United States','New York','Yes','America/New_York','3600000');";
	String america10 = "INSERT INTO " + entriesTable + " Values (37,'United States','Louisville','Yes','America/Kentucky/Louisville','3600000');";
	String america11 = "INSERT INTO " + entriesTable + " Values (38,'United States','Fort Wayne','Yes','America/Fort_Wayne','3600000');";
	String america12 = "INSERT INTO " + entriesTable + " Values (39,'United States','Knoxville','Yes','America/Indiana/Knox','0');";
	

	db.execSQL(america1);
	db.execSQL(america2);
	db.execSQL(america3);
	db.execSQL(america4);
	db.execSQL(america5);
	db.execSQL(america6);
	db.execSQL(america7);
	db.execSQL(america8);
	db.execSQL(america9);
	db.execSQL(america10);
	db.execSQL(america11);
	db.execSQL(america12);

		
	String metric1 = "INSERT INTO " + allmetricsTable + " Values (1,'feet to/from meter');";
	String metric2 = "INSERT INTO " + allmetricsTable + " Values (2,'mile to/from km');";
	String metric3 = "INSERT INTO " + allmetricsTable + " Values (3,'ounce to/from gram');";
	String metric4 = "INSERT INTO " + allmetricsTable + " Values (4,'pound to/from kilogram');";
	String metric5 = "INSERT INTO " + allmetricsTable + " Values (5,'fluid ounce to/from ml');";
	String metric6 = "INSERT INTO " + allmetricsTable + " Values (6,'gallon to/from liter');";
	
	
	db.execSQL(metric1);
	db.execSQL(metric2);
	db.execSQL(metric3);
	db.execSQL(metric4);
	db.execSQL(metric5);
	db.execSQL(metric6);
	

/*	private final String metricoriginalID = "metricoriginalID";
	private final String metricName1toName2 = "name1to2";
	private final String metricName1toName2value = "name1to2value";
	private final String metricName2toName1 = "name2to1";
	private final String metricName2toName1value = "name2to1value";
	*/
	
	String metric11 = "INSERT INTO " + metricTable + " Values (1,'feet to meter','0.3048','meter to feet','3.28084');";
	String metric22 = "INSERT INTO " + metricTable + " Values (2,'mile to km','1.60934','km to mile','0.621371');";
	String metric33 = "INSERT INTO " + metricTable + " Values (3,'ounce to gram','28.3495','gram to ounce','0.035274');";
	String metric44 = "INSERT INTO " + metricTable + " Values (4,'pound to kilogram','0.453592','kilogram to pound','2.20462');";
	String metric55 = "INSERT INTO " + metricTable + " Values (5,'fluid ounce to ml','29.5735','ml to fluid ounce','0.033814');";
	String metric66 = "INSERT INTO " + metricTable + " Values (6,'gallon to liter','3.78541','liter to gallon','0.264172');";
	
	db.execSQL(metric11);
	db.execSQL(metric22);
	db.execSQL(metric33);
	db.execSQL(metric44);
	db.execSQL(metric55);
	db.execSQL(metric66);
	
	
	
	/*	 Aruba
	 * australia
	 * canada
	 * china
	 * france
	 * Great britian
	 * italy japan 
	 * mexico 
	 * newzealand
	 * Singapore 
	 * usa
*/
	
}

//this method is used if the DatabaseHelper constructor changes numbers, if it does, then it deleats the old database and makes the new one

@Override
public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	db.execSQL("DROP TABLE IF EXISTS "+ countriesTable);
	db.execSQL("DROP TABLE IF EXISTS "+ allcountriesTable);
	db.execSQL("DROP TABLE IF EXISTS "+ allmetricsTable);
	onCreate(db);
	
}

public void UpdateCurrencyConversiontoUSD(String country,String newvalue)
{
	   SQLiteDatabase db = this.getWritableDatabase();
	 
	    ContentValues values = new ContentValues();
	    values.put(CurrencyConversiontoUSD, newvalue);
	 
	    // updating row
	     db.update(countriesTable, values, colID + " = ?",
	            new String[] { String.valueOf(country) });

}
public void UpdateCurrencyConversionfromUSD(String country,String newvalue)
{
	   SQLiteDatabase db = this.getWritableDatabase();
	 
	    ContentValues values = new ContentValues();
	    values.put(CurrencyConversionfromUSD, newvalue);
	 
	    // updating row
	     db.update(countriesTable, values, colID + " = ?",
	            new String[] { String.valueOf(country) });

}

//this getter ques the currecnyType for the origin country
public String getOriginCurrencyType(){
	SQLiteDatabase db = this.getReadableDatabase();
	Cursor cur = db.rawQuery("SELECT "+colCurrencyType+" FROM "+ countriesTable +" WHERE "+colID+" LIKE '"+ originCountry +"'", new String[] {});
	cur.moveToFirst();		
	return cur.getString(0);
}

//this getter ques the currecnyType for the origin country
public String getDestinationCurrencyType(){
	SQLiteDatabase db = this.getReadableDatabase();
	Cursor cur = db.rawQuery("SELECT "+colCurrencyType+" FROM "+ countriesTable +" WHERE "+colID+" LIKE '"+ destinationCountry +"'", new String[] {});
	cur.moveToFirst();		
	return cur.getString(0);
}

//this getter ques the currecnyType for the origin country
public String getOriginCurrencyConversion(){
	SQLiteDatabase db = this.getReadableDatabase();
	Cursor cur = db.rawQuery("SELECT "+CurrencyConversionfromUSD+" FROM "+ countriesTable +" WHERE "+colID+" LIKE '"+ originCountry +"'", new String[] {});
	cur.moveToFirst();		
	return cur.getString(0);
}
/*private final String metricoriginalID = "metricoriginalID";
private final String metricName1toName2 = "name1to2";
private final String metricName1toName2value = "name1to2value";
private final String metricName2toName1 = "name2to1";
private final String metricName2toName1value = "name2to1value";
*/
public String getName1toName2(String check)
{ // Getting Event id
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(metricTable, new String[] { metricoriginalID,
                metricName1toName2, metricName1toName2value,
                metricName2toName1, metricName2toName1value,
                        
        }, metricName1toName2 + "=?",
                new String[] { String.valueOf(check) }, null, null, null, null);
        if (cursor != null)
        {    cursor.moveToFirst();
 
             n1ton2value =   cursor.getString(2);
             
             db.close();
        }
        
	return n1ton2value;
}
public String getName2toName1(String check)
{ // Getting Event id
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(metricTable, new String[] { metricoriginalID,
                metricName1toName2, metricName1toName2value,
                metricName2toName1, metricName2toName1value,
                        
        }, metricName2toName1 + "=?",
                new String[] { String.valueOf(check) }, null, null, null, null);
        if (cursor != null)
        {    cursor.moveToFirst();
 
             n2ton1value =   cursor.getString(4);
             
             db.close();
        }
        
	return n2ton1value;
}

public int getCountryName(String check)
{ // Getting Event id
        SQLiteDatabase db = this.getReadableDatabase();
 int eventid=45;
        Cursor cursor = db.query(allcountriesTable, new String[] { countryID,
                countryName }, countryName + "=?",
                new String[] { String.valueOf(check) }, null, null, null, null);
        if (cursor != null)
        {    cursor.moveToFirst();
 
             eventid =   Integer.parseInt(cursor.getString(1));
             
             db.close();
    }
        
	return eventid;
}


public List<AllEntries> getAllEntries() {
    List<AllEntries> friendList = new ArrayList<AllEntries>();
    // Select All Query
    String selectQuery = "SELECT  * FROM " +entriesTable;
 
    SQLiteDatabase db = this.getWritableDatabase();
    Cursor cursor = db.rawQuery(selectQuery, null);
 
    // looping through all rows and adding to list
    if (cursor.moveToFirst()) {
        do {
        	AllEntries entry = new AllEntries();
            entry.set_countryID(Integer.parseInt(cursor.getString(0)));
            entry.set_countryName((cursor.getString(1)));
            entry.set_cityValue((cursor.getString(2)));
            entry.set_multipletime((cursor.getString(3)));
            entry.set_exactstring((cursor.getString(4)));
            // Adding contact to list
            friendList.add(entry);
        } while (cursor.moveToNext());
    }
 
    // return Friend list
    return friendList;
}
public List<String> checktimezone(String countryName){
    List<String> labels = new ArrayList<String>();
     
    // Select All Query
    //String selectQuery = "SELECT  * FROM " + entriesTable;
  
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.query(entriesTable, new String[] {
    		entries_countryID,
    		entries_countryName,
    		entries_countryValue,
    		entries_multipletime,
    		entries_string
             }, entries_countryName + "=?",
            new String[] { String.valueOf(countryName) }, null, null, null, null);
    // looping through all rows and adding to list
    if (cursor.moveToFirst()) {
        do {
        	counter = counter +1;
        	/*AllEntries entry = new AllEntries();
            entry.set_countryID(Integer.parseInt(cursor.getString(0)));
            entry.set_countryName((cursor.getString(1)));
            entry.set_cityValue((cursor.getString(2)));
            entry.set_multipletime((cursor.getString(3)));
            entry.set_exactstring((cursor.getString(4)));
           */
        	labels.add(cursor.getString(3));
        } while (cursor.moveToNext());
    }
     
    // closing connection
    cursor.close();
    db.close();
     
    // returning lables
    return labels;
}

public List<String> getAllEntriesString(String countryName){
    List<String> labels = new ArrayList<String>();
     
    // Select All Query
    //String selectQuery = "SELECT  * FROM " + entriesTable;
  
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.query(entriesTable, new String[] {
    		entries_countryID,
    		entries_countryName,
    		entries_countryValue,
    		entries_multipletime,
    		entries_string,entries_plus
             }, entries_countryName + "=?",
            new String[] { String.valueOf(countryName) }, null, null, null, null);
    // looping through all rows and adding to list
    if (cursor.moveToFirst()) {
        do {
        	counter = counter +1;
        	/*AllEntries entry = new AllEntries();
            entry.set_countryID(Integer.parseInt(cursor.getString(0)));
            entry.set_countryName((cursor.getString(1)));
            entry.set_cityValue((cursor.getString(2)));
            entry.set_multipletime((cursor.getString(3)));
            entry.set_exactstring((cursor.getString(4)));
           */
        	labels.add(cursor.getString(2));
        } while (cursor.moveToNext());
    }
     
    // closing connection
    cursor.close();
    db.close();
     
    // returning lables
    return labels;
}

public List<String> getstring(String cityValue){
    List<String> labels = new ArrayList<String>();
     
    // Select All Query
    //String selectQuery = "SELECT  * FROM " + entriesTable;
  
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.query(entriesTable, new String[] {
    		entries_countryID,
    		entries_countryName,
    		entries_countryValue,
    		entries_multipletime,
    		entries_string
             }, entries_countryValue + "=?",
            new String[] { String.valueOf(cityValue) }, null, null, null, null);
    // looping through all rows and adding to list
    if (cursor.moveToFirst()) {
        do {
        	counter = counter +1;
        	/*AllEntries entry = new AllEntries();
            entry.set_countryID(Integer.parseInt(cursor.getString(0)));
            entry.set_countryName((cursor.getString(1)));
            entry.set_cityValue((cursor.getString(2)));
            entry.set_multipletime((cursor.getString(3)));
            entry.set_exactstring((cursor.getString(4)));
           */
        	labels.add(cursor.getString(4));
        } while (cursor.moveToNext());
    }
     
    // closing connection
    cursor.close();
    db.close();
     
    // returning lables
    return labels;
}
public String getstringplus(String cityValue){
    
     
    // Select All Query
    //String selectQuery = "SELECT  * FROM " + entriesTable;
  
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.query(entriesTable, new String[] {
    		entries_countryID,
    		entries_countryName,
    		entries_countryValue,
    		entries_multipletime,
    		entries_string,entries_plus
             }, entries_countryValue + "=?",
            new String[] { String.valueOf(cityValue) }, null, null, null, null);
    // looping through all rows and adding to list
    if (cursor.moveToFirst()) {
        do {
        	counter = counter +1;
        	/*AllEntries entry = new AllEntries();
            entry.set_countryID(Integer.parseInt(cursor.getString(0)));
            entry.set_countryName((cursor.getString(1)));
            entry.set_cityValue((cursor.getString(2)));
            entry.set_multipletime((cursor.getString(3)));
            entry.set_exactstring((cursor.getString(4)));
           */
        	Log.d("plusstringactualvalue",cursor.getString(5));
        	labels  = cursor.getString(5);
        } while (cursor.moveToNext());
    }
     
    // closing connection
    cursor.close();
    db.close();
     
    // returning lables
    return labels;
}
public List<AllEntries>  getSelectedEntries(String countryName) {
    List<AllEntries> friendList = new ArrayList<AllEntries>();
    // Select All Query
     SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.query(entriesTable, new String[] {
    		entries_countryID,
    		entries_countryName,
    		entries_countryValue,
    		entries_multipletime,
    		entries_string
             }, entries_countryName + "=?",
            new String[] { String.valueOf(countryName) }, null, null, null, null);
    // looping through all rows and adding to list
    if (cursor.moveToFirst()) {
        do {
        	counter = counter +1;
        	AllEntries entry = new AllEntries();
            entry.set_countryID(Integer.parseInt(cursor.getString(0)));
            entry.set_countryName((cursor.getString(1)));
            entry.set_cityValue((cursor.getString(2)));
            entry.set_multipletime((cursor.getString(3)));
            entry.set_exactstring((cursor.getString(4)));
            // Adding contact to list
            friendList.add(entry);
        } while (cursor.moveToNext());
    }
    Log.d(countryName.toString()+" counter",String.valueOf(counter));
    counter=0;
    // return Friend list
    return friendList;
}




/*
public void updateCurrency(String oldeventname,String neweventname) {
	//    SQLiteDatabase db = this.getWritableDatabase();
	   // db.execSQL("UPDATE "+EVENTS_TABLE+" SET "+EVENTS_NAME+"="+neweventname +" WHERE "+EVENTS_NAME+"="+oldeventname);
	  
		SQLiteDatabase db = this.getWritableDatabase();
		 
	    ContentValues values = new ContentValues();
	    values.put(CURRENCY_VALUE, neweventname);
	    // updating row
	    db.update(CURRENCY_TABLE, values, CURRENCY_VALUE + " = ?",
	            new String[] { String.valueOf(oldeventname) });
	    
	       
	   
	}
public void addDPExpense(DisplayExpense e) {
    SQLiteDatabase db = this.getWritableDatabase();

    ContentValues values = new ContentValues();
    values.put(DP_EXPENSE_EVENT_NAME, e.get_eventname_dp());
    values.put(DP_EXPENSE_NAME, e.get_expensename_dp()); 
    values.put(DP_EXPENSE_DATE, e.get_date_dp()); // 
    values.put(DP_EXPENSE_PAID_BY,e.get_paid_by_dp()); // 
    values.put(DP_EXPENSE_PRICE, e.get_price_dp()); // 
    values.put(DP_EXPENSE_COUNT, e.get_count_dp());
    values.put(DP_EXPENSE_TYPE, e.get_type_dp());// 
 
    // Inserting Row
    db.insert(DP_EXPENSE_TABLE, null, values);
   db.close(); // Closing database connection
}
*/
//this getter ques the currecnyType for the origin country
public String getDestinationCurrencyConversion(){
	SQLiteDatabase db = this.getReadableDatabase();
	Cursor cur = db.rawQuery("SELECT "+CurrencyConversionfromUSD+" FROM "+ countriesTable +" WHERE "+colID+" LIKE '"+ destinationCountry +"'", new String[] {});
	cur.moveToFirst();		
	return cur.getString(0);
}

//this getter ques the currecnyType for the origin country
public String getOriginTemperature(){
	SQLiteDatabase db = this.getReadableDatabase();
	Cursor cur = db.rawQuery("SELECT "+colTemperature+" FROM "+ countriesTable +" WHERE "+colID+" LIKE '"+ originCountry +"'", new String[] {});
	cur.moveToFirst();		
	return cur.getString(0);
}

//this getter ques the currecnyType for the origin country
public String getDestinationTemperature(){
	SQLiteDatabase db = this.getReadableDatabase();
	Cursor cur = db.rawQuery("SELECT "+colTemperature+" FROM "+ countriesTable +" WHERE "+colID+" LIKE '"+ destinationCountry +"'", new String[] {});
	cur.moveToFirst();		
	return cur.getString(0);
}

//this getter ques the currecnyType for the origin country
public String getOriginLanguages(){
	SQLiteDatabase db = this.getReadableDatabase();
	Cursor cur = db.rawQuery("SELECT "+colLanguages+" FROM "+ countriesTable +" WHERE "+colID+" LIKE '"+ originCountry +"'", new String[] {});
	cur.moveToFirst();		
	return cur.getString(0);
}

//this getter ques the currecnyType for the origin country
public String getDestinationLanguages(){
	SQLiteDatabase db = this.getReadableDatabase();
	Cursor cur = db.rawQuery("SELECT "+colLanguages+" FROM "+ countriesTable +" WHERE "+colID+" LIKE '"+ destinationCountry +"'", new String[] {});
	cur.moveToFirst();		
	return cur.getString(0);
}

//this getter ques the currecnyType for the origin country
public String getOriginPlugType(){
	SQLiteDatabase db = this.getReadableDatabase();
	Cursor cur = db.rawQuery("SELECT "+colPlugType+" FROM "+ countriesTable +" WHERE "+colID+" LIKE '"+ originCountry +"'", new String[] {});
	cur.moveToFirst();		
	return cur.getString(0);
}
public String getOriginFrequency(){
	SQLiteDatabase db = this.getReadableDatabase();
	Cursor cur = db.rawQuery("SELECT "+colPlugTypeFreq+" FROM "+ countriesTable +" WHERE "+colID+" LIKE '"+ originCountry +"'", new String[] {});
	cur.moveToFirst();		
	return cur.getString(0);
}
//this getter ques the currecnyType for the origin country
public String getDestinationPlugType(){
	SQLiteDatabase db = this.getReadableDatabase();
	Cursor cur = db.rawQuery("SELECT "+colPlugType+" FROM "+ countriesTable +" WHERE "+colID+" LIKE '"+ destinationCountry +"'", new String[] {});
	cur.moveToFirst();		
	return cur.getString(0);
}
public String getDestinationFrequency(){
	SQLiteDatabase db = this.getReadableDatabase();
	Cursor cur = db.rawQuery("SELECT "+colPlugTypeFreq+" FROM "+ countriesTable +" WHERE "+colID+" LIKE '"+ destinationCountry +"'", new String[] {});
	cur.moveToFirst();		
	return cur.getString(0);
}

//this getter ques the currecnyType for the origin country
public String getOriginPlugTypeVolt(){
	SQLiteDatabase db = this.getReadableDatabase();
	Cursor cur = db.rawQuery("SELECT "+colPlugTypeVolt+" FROM "+ countriesTable +" WHERE "+colID+" LIKE '"+ originCountry +"'", new String[] {});
	cur.moveToFirst();		
	return cur.getString(0);
}

//this getter ques the currecnyType for the origin country
public String getDestinationPlugTypeVolt(){
	SQLiteDatabase db = this.getReadableDatabase();
	Cursor cur = db.rawQuery("SELECT "+colPlugTypeVolt+" FROM "+ countriesTable +" WHERE "+colID+" LIKE '"+ destinationCountry +"'", new String[] {});
	cur.moveToFirst();		
	return cur.getString(0);
}

//this getter ques the currecnyType for the origin country
public String getOriginPlugTypeFreq(){
	SQLiteDatabase db = this.getReadableDatabase();
	Cursor cur = db.rawQuery("SELECT "+colPlugTypeFreq+" FROM "+ countriesTable +" WHERE "+colID+" LIKE '"+ originCountry +"'", new String[] {});
	cur.moveToFirst();		
	return cur.getString(0);
}

//this getter ques the currecnyType for the origin country
public String getDestinationPlugTypeFreq(){
	SQLiteDatabase db = this.getReadableDatabase();
	Cursor cur = db.rawQuery("SELECT "+colPlugTypeFreq+" FROM "+ countriesTable +" WHERE "+colID+" LIKE '"+ destinationCountry +"'", new String[] {});
	cur.moveToFirst();		
	return cur.getString(0);
}

//this getter ques the currecnyType for the origin country
public String getOriginMeasurements(){
	SQLiteDatabase db = this.getReadableDatabase();
	Cursor cur = db.rawQuery("SELECT "+colMeasurements+" FROM "+ countriesTable +" WHERE "+colID+" LIKE '"+ originCountry +"'", new String[] {});
	cur.moveToFirst();		
	return cur.getString(0);
}

//this getter ques the currecnyType for the origin country
public String getDestinationMeasurements(){
	SQLiteDatabase db = this.getReadableDatabase();
	Cursor cur = db.rawQuery("SELECT "+colMeasurements+" FROM "+ countriesTable +" WHERE "+colID+" LIKE '"+ destinationCountry +"'", new String[] {});
	cur.moveToFirst();		
	return cur.getString(0);
}

//this getter ques the currecnyType for the origin country
public String getOriginCommonMeasure(){
	SQLiteDatabase db = this.getReadableDatabase();
	Cursor cur = db.rawQuery("SELECT "+colCommonMeasure+" FROM "+ countriesTable +" WHERE "+colID+" LIKE '"+ originCountry +"'", new String[] {});
	cur.moveToFirst();		
	return cur.getString(0);
}

//this getter ques the currecnyType for the origin country
public String getDestinationCommonMeasure(){
	SQLiteDatabase db = this.getReadableDatabase();
	Cursor cur = db.rawQuery("SELECT "+colCommonMeasure+" FROM "+ countriesTable +" WHERE "+colID+" LIKE '"+ destinationCountry +"'", new String[] {});
	cur.moveToFirst();		
	return cur.getString(0);
}

}
