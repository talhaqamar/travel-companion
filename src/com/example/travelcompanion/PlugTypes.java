package com.example.travelcompanion;

import java.util.regex.Pattern;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.amazon.device.associates.AssociatesAPI;
import com.amazon.device.associates.LinkService;
import com.amazon.device.associates.NotInitializedException;
import com.amazon.device.associates.OpenSearchPageRequest;
import com.amazon.device.associates.SortType;
import com.fitscholar.travelcompanion.R;

/* This class is a display class that shows plug type information for both the origin and destination country  */ 

public class PlugTypes extends Fragment {
	//the following are viewtype items
    TextView plug_types_destination;
    TextView plug_types_plug;
    TextView plug_types_A;
    ImageView plug_types_picture_A;
    TextView plug_types_description_A;
    TextView plug_types_B;
    ImageView plug_types_picture_B;
    TextView plug_types_description_B;
    TextView plug_types_C;
    ImageView plug_types_picture_C;
    TextView plug_types_description_C;
    TextView plug_types_E;
    ImageView plug_types_picture_E;
    TextView plug_types_description_E;
    TextView plug_types_F;
    ImageView plug_types_picture_F;
    TextView plug_types_description_F;
    TextView plug_types_G;
    ImageView plug_types_picture_G;
    TextView plug_types_description_G;
    TextView plug_types_I;
    ImageView plug_types_picture_I;
    TextView plug_types_description_I;
    TextView plug_types_L;
    ImageView plug_types_picture_L;
    TextView plug_types_description_L;
    // Destination
    TextView plug_types_M;
    ImageView plug_types_picture_M;
    TextView plug_types_description_M;
    TextView plug_types_N;
    ImageView plug_types_picture_N;
    TextView plug_types_description_N;
    TextView plug_types_O;
    ImageView plug_types_picture_O;
    TextView plug_types_description_O;
    TextView plug_types_P;
    ImageView plug_types_picture_P;
    TextView plug_types_description_P;
    TextView plug_types_Q;
    ImageView plug_types_picture_Q;
    TextView plug_types_description_Q;
    TextView plug_types_R;
    ImageView plug_types_picture_R;
    TextView plug_types_description_R;
    TextView plug_types_S;
    ImageView plug_types_picture_S;
    TextView plug_types_description_S;
    TextView plug_types_T;
    ImageView plug_types_picture_T;
    TextView plug_types_description_T;
    
    TextView firstlinktoapi;
    TextView secondlinktoapi;
    TextView nextpagelink;
    
    TextView plug_types_display;
    TextView tableRowDestination_text,tableRowOrigin_text;
    TextView tableRowDestination_text_freq,tableRowOrigin_text_freq;
    static String appkey = "56c733f5bea142a3bb8b6366c9b90ef2";
    OpenSearchPageRequest osprequest;
    MainActivity activity;
    public PlugTypes() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {

          View view = inflater.inflate(R.layout.plug_types_layout, container,
                      false);

          //the following makes all viewtype variables visable in the feild
          plug_types_destination = (TextView) view.findViewById(R.id.plug_types_destination);
          tableRowDestination_text = (TextView)view.findViewById(R.id.tableRowDesination_text);
          tableRowOrigin_text =(TextView)view.findViewById(R.id.tableRowOrigin_text);
          plug_types_A = (TextView) view.findViewById(R.id.plug_types_A);
          plug_types_picture_A = (ImageView) view.findViewById(R.id.plug_types_picture_A);
          plug_types_description_A = (TextView) view.findViewById(R.id.plug_types_description_A);
          plug_types_B = (TextView) view.findViewById(R.id.plug_types_B);
          plug_types_picture_B = (ImageView) view.findViewById(R.id.plug_types_picture_B);
          plug_types_description_B = (TextView) view.findViewById(R.id.plug_types_description_B);
          plug_types_C = (TextView) view.findViewById(R.id.plug_types_C);
          plug_types_picture_C = (ImageView) view.findViewById(R.id.plug_types_picture_C);
          plug_types_description_C = (TextView) view.findViewById(R.id.plug_types_description_C);
          plug_types_E = (TextView) view.findViewById(R.id.plug_types_E);
          plug_types_picture_E = (ImageView) view.findViewById(R.id.plug_types_picture_E);
          plug_types_description_E = (TextView) view.findViewById(R.id.plug_types_description_E);
          plug_types_F = (TextView) view.findViewById(R.id.plug_types_F);
          plug_types_picture_F = (ImageView) view.findViewById(R.id.plug_types_picture_F);
          plug_types_description_F = (TextView) view.findViewById(R.id.plug_types_description_F);
          plug_types_G = (TextView) view.findViewById(R.id.plug_types_G);
          plug_types_picture_G = (ImageView) view.findViewById(R.id.plug_types_picture_G);
          plug_types_description_G = (TextView) view.findViewById(R.id.plug_types_description_G);
          plug_types_I = (TextView) view.findViewById(R.id.plug_types_I);
          plug_types_picture_I = (ImageView) view.findViewById(R.id.plug_types_picture_I);
          plug_types_description_I = (TextView) view.findViewById(R.id.plug_types_description_I);
          plug_types_L = (TextView) view.findViewById(R.id.plug_types_L);
          plug_types_picture_L = (ImageView) view.findViewById(R.id.plug_types_picture_L);
          plug_types_description_L = (TextView) view.findViewById(R.id.plug_types_description_L);
          //Destination
          
          plug_types_M = (TextView) view.findViewById(R.id.plug_types_M);
          plug_types_picture_M = (ImageView) view.findViewById(R.id.plug_types_picture_M);
          plug_types_description_M = (TextView) view.findViewById(R.id.plug_types_description_M);
          plug_types_N = (TextView) view.findViewById(R.id.plug_types_N);
          plug_types_picture_N = (ImageView) view.findViewById(R.id.plug_types_picture_N);
          plug_types_description_N = (TextView) view.findViewById(R.id.plug_types_description_N);
          plug_types_O = (TextView) view.findViewById(R.id.plug_types_O);
          plug_types_picture_O = (ImageView) view.findViewById(R.id.plug_types_picture_O);
          plug_types_description_O = (TextView) view.findViewById(R.id.plug_types_description_O);
          plug_types_P = (TextView) view.findViewById(R.id.plug_types_P);
          plug_types_picture_P = (ImageView) view.findViewById(R.id.plug_types_picture_P);
          plug_types_description_P = (TextView) view.findViewById(R.id.plug_types_description_P);
          plug_types_Q = (TextView) view.findViewById(R.id.plug_types_Q);
          plug_types_picture_Q = (ImageView) view.findViewById(R.id.plug_types_picture_Q);
          plug_types_description_Q = (TextView) view.findViewById(R.id.plug_types_description_Q);
          plug_types_R = (TextView) view.findViewById(R.id.plug_types_R);
          plug_types_picture_R = (ImageView) view.findViewById(R.id.plug_types_picture_R);
          plug_types_description_R = (TextView) view.findViewById(R.id.plug_types_description_R);
          plug_types_S = (TextView) view.findViewById(R.id.plug_types_S);
          plug_types_picture_S = (ImageView) view.findViewById(R.id.plug_types_picture_S);
          plug_types_description_S = (TextView) view.findViewById(R.id.plug_types_description_S);
          plug_types_T = (TextView) view.findViewById(R.id.plug_types_T);
          plug_types_picture_T = (ImageView) view.findViewById(R.id.plug_types_picture_T);
          plug_types_description_T = (TextView) view.findViewById(R.id.plug_types_description_T);
          firstlinktoapi= (TextView) view.findViewById(R.id.firstlinktoapi);
          secondlinktoapi= (TextView) view.findViewById(R.id.secondlinktoapi);
          nextpagelink= (TextView) view.findViewById(R.id.nextpagelink);
          
          tableRowDestination_text_freq=(TextView)view.findViewById(R.id.tableRowDestination_text_freq);
          tableRowOrigin_text_freq=(TextView)view.findViewById(R.id.tableRowOrigin_text_freq);
          plug_types_display = (TextView) view.findViewById(R.id.plug_types_display);

          //the following code gets the origin and destination from this fragments activity, MainActivity
           activity = (MainActivity) getActivity();
          String origin = activity.getOrigin();
          String destination = activity.getDestination();
          
          AssociatesAPI.initialize(new AssociatesAPI.Config(appkey, activity));
          DatabaseHelper dbHelper = new DatabaseHelper(activity);
          
          //this code sets viewable text views to desired output
          String plugTypesString = dbHelper.getOriginPlugType();
          String plugTypeVolt = dbHelper.getOriginPlugTypeVolt();
          String plugTypeFreq = dbHelper.getOriginPlugTypeFreq();
          String[] indTypes = plugTypesString.split(",");
          
          String adapTypesString = dbHelper.getDestinationPlugType();// plug type
          String adaptypesfreq = dbHelper.getDestinationFrequency();// frequency
          String adapTypesVolt = dbHelper.getDestinationPlugTypeVolt(); //
          
          String adapTypesFreq = dbHelper.getDestinationPlugType();
           String[] adapTypes = adapTypesString.split(",");
          dbHelper.close();
          tableRowOrigin_text_freq.setText("Plug types : "+plugTypesString+"\n"+"Voltage : " +plugTypeVolt+"V"+"\n"+"Frequency : "+plugTypeFreq+"Hz");
          tableRowDestination_text_freq.setText("Plug types : "+adapTypesString+"\n"+"Voltage : " +adapTypesVolt+"V"+"\n"+"Frequency : "+adaptypesfreq+"Hz");
          
          String mystring=new String("Purchase Plug Adapters on Amazon.com");
          SpannableString content = new SpannableString(mystring);
          content.setSpan(new UnderlineSpan(), 0, mystring.length(), 0);
          firstlinktoapi.setText(content);
          firstlinktoapi.setTextColor(Color.parseColor("#33B5E5"));
         firstlinktoapi.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				boolean check = isOnline(activity);
				if(check){
			 osprequest = new OpenSearchPageRequest("Electronics", "international plug adapters");
			osprequest.setBrand("Amazon");
			osprequest.setSortType(SortType.BESTSELLING);
				try {
					LinkService	linkservice = AssociatesAPI.getLinkService();
					linkservice.openRetailPage(osprequest);
				} catch (NotInitializedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				}
				else 
				{
					Toast.makeText(activity, "Turn on the wifi then try again!!!", 5000).show();
				}
			}
		});
         String mystring1=new String("Purchase Converters/Transformers on Amazon.com");
         SpannableString content1 = new SpannableString(mystring1);
         content1.setSpan(new UnderlineSpan(), 0, mystring1.length(), 0);
         secondlinktoapi.setText(content1);
         secondlinktoapi.setTextColor(Color.parseColor("#33B5E5"));
     
          secondlinktoapi.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				boolean check = isOnline(activity);
		if(check)
		{
			osprequest = new OpenSearchPageRequest("Electronics", "220/240V to 110/120V voltage step up step down universal travel converter");
			osprequest.setBrand("Amazon");
			osprequest.setSortType(SortType.BESTSELLING);
				try {
					LinkService	linkservice = AssociatesAPI.getLinkService();
					linkservice.openRetailPage(osprequest);
				} catch (NotInitializedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				}
				else 
				{
					Toast.makeText(activity, "Turn on the wifi then try again!!!", 5000).show();
				}
			}
		});
          String nextlink = "<html><a href=''>Learn more about Plug Types, Voltage and Frequency</a></html>";
          nextpagelink.setText(Html.fromHtml(nextlink));
          nextpagelink.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
			Fragment fragment= new NextPlugTypes();
			        FragmentManager frgManager = getFragmentManager();
			            frgManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
			 }
		});
          TableLayout table = (TableLayout)view.findViewById(R.id.plugTable);
         
       

          for (int i = 0; i< indTypes.length; i++){
        	  if(indTypes[i].matches("A")){
        		  TableRow row = (TableRow)view.findViewById(R.id.tableRowA);
                  row.setVisibility(0);
        	  }else if (indTypes[i].matches("B")){
        		  TableRow row = (TableRow)view.findViewById(R.id.tableRowB);
                  row.setVisibility(0);
        	  }else if (indTypes[i].matches("C")){
        		  TableRow row = (TableRow)view.findViewById(R.id.tableRowC);
                  row.setVisibility(0);
        	  }else if (indTypes[i].matches("E")){
        		  TableRow row = (TableRow)view.findViewById(R.id.tableRowE);
                  row.setVisibility(0);
        	  }else if (indTypes[i].matches("F")){
        		  TableRow row = (TableRow)view.findViewById(R.id.tableRowF);
                  row.setVisibility(0);
        	  }else if (indTypes[i].matches("G")){
        		  TableRow row = (TableRow)view.findViewById(R.id.tableRowG);
                  row.setVisibility(0);
        	  }else if (indTypes[i].matches("I")){
        		  TableRow row = (TableRow)view.findViewById(R.id.tableRowI);
                  row.setVisibility(0);
        	  }else if (indTypes[i].matches("L")){
        		  TableRow row = (TableRow)view.findViewById(R.id.tableRowL);
                  row.setVisibility(0);
        	  }
        	 	 tableRowOrigin_text.setText(origin);
        		  TableRow rowd = (TableRow)view.findViewById(R.id.tableRowDestination);
        		  rowd.setVisibility(0);
        	 
          }
          for (int j =0; j< adapTypes.length; j++){
        	  if(adapTypes[j].matches("A")){
        		  TableRow row = (TableRow)view.findViewById(R.id.tableRowM);
                  row.setVisibility(0);
        	  }else if (adapTypes[j].matches("B")){
        		  TableRow row = (TableRow)view.findViewById(R.id.tableRowN);
                  row.setVisibility(0);
        	  }else if (adapTypes[j].matches("C")){
        		  TableRow row = (TableRow)view.findViewById(R.id.tableRowO);
                  row.setVisibility(0);
        	  }else if (adapTypes[j].matches("E")){
        		  TableRow row = (TableRow)view.findViewById(R.id.tableRowP);
                  row.setVisibility(0);
        	  }else if (adapTypes[j].matches("F")){
        		  TableRow row = (TableRow)view.findViewById(R.id.tableRowQ);
                  row.setVisibility(0);
        	  }else if (adapTypes[j].matches("G")){
        		  TableRow row = (TableRow)view.findViewById(R.id.tableRowR);
                  row.setVisibility(0);
        	  }else if (adapTypes[j].matches("I")){
        		  TableRow row = (TableRow)view.findViewById(R.id.tableRowS);
                  row.setVisibility(0);
        	  }else if (adapTypes[j].matches("L")){
        		  TableRow row = (TableRow)view.findViewById(R.id.tableRowT);
                  row.setVisibility(0);
        	  }
        	    tableRowDestination_text.setText(destination);
        		  TableRow rowq = (TableRow)view.findViewById(R.id.tableRowDestination);
        		  rowq.setVisibility(0);
        	 
          } 
         
       //   rowd.setVisibility(0);
         
         
        //  rowq.setVisibility(0);
          
          plug_types_display.setText(origin + " - > " + destination);
          return view;
    }
    public boolean isOnline(Activity a) {
	    ConnectivityManager cm =
	        (ConnectivityManager) a.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
	        return true;
	    }
	    return false;
	}
}
