package com.example.travelcompanion;

import java.text.DecimalFormat;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.fitscholar.travelcompanion.R;

/* This class is a calculator that allows the user to put in a price for either the origin or destination country and then 
 * see the equivelent of the respective other countries monitization  */ 

public class Currency extends Fragment {
	//the following are viewtype items 
    TextView currency_origin;
    EditText currency_origin_number;
    TextView currency_origin_label;
    TextView currency_destination;
    EditText currency_destination_number;
    TextView currency_destination_label;
    Button button;
    Button clear;
    TextView currency_instructions;
    TextView currency_display;


    public Currency() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {

          View view = inflater.inflate(R.layout.currency_layout, container,
                      false);
          
          DatabaseHelper dbHelper = new DatabaseHelper(getActivity());
          final Double originConversion= Double.parseDouble(dbHelper.getOriginCurrencyConversion());
          final Double destinationConversion = Double.parseDouble(dbHelper.getDestinationCurrencyConversion());
          
          //the following makes all viewtype variables visable in the feild 
          currency_origin = (TextView) view.findViewById(R.id.currency_origin);
          currency_origin_number = (EditText) view.findViewById(R.id.currency_origin_number);
          currency_origin_number.setOnFocusChangeListener(new OnFocusChangeListener() {
        	  @Override
        	  public void onFocusChange(View v, boolean hasFocus) {
        		  if(hasFocus){
        			  currency_origin_number.setText("");
        			  currency_destination_number.setText("");
        		  }else {
        			  currency_origin_number.setText("");
        			  currency_destination_number.setText("");
        		  }
        	  }
          });
          currency_origin_label = (TextView) view.findViewById(R.id.currency_origin_label);
          currency_destination = (TextView) view.findViewById(R.id.currency_destination);
          currency_destination_number = (EditText) view.findViewById(R.id.currency_destination_number);
          currency_destination_number.setOnFocusChangeListener(new OnFocusChangeListener() {
        	  @Override
        	  public void onFocusChange(View v, boolean hasFocus) {
        		  if(hasFocus){
        			  currency_destination_number.setText("");
        			  currency_origin_number.setText("");
        		  }else {
        			  currency_origin_number.setText("");
        			  currency_destination_number.setText("");
        		  }
        	  }
          });
          currency_destination_label = (TextView) view.findViewById(R.id.currency_destination_label);
          final DecimalFormat df = new DecimalFormat("#######.00");
          button = (Button) view.findViewById(R.id.currency_go);
          button.setOnClickListener(new View.OnClickListener() {
        	    public void onClick(View v) {
        	    	String originCurrency = currency_origin_number.getText().toString();
        	    	String destinationCurrency = currency_destination_number.getText().toString();
        	    	if(originCurrency.matches("") && !destinationCurrency.matches("")){	    		
        	    		String originTotal = String.valueOf(df.format((((Double.parseDouble(destinationCurrency))/destinationConversion) * originConversion)));
        	    		currency_origin_number.setText(originTotal);
        	    	}
        	    	else if (destinationCurrency.matches("") && !originCurrency.matches("")){
            	    	String destinationTotal = String.valueOf(df.format((((Double.parseDouble(originCurrency))/originConversion) * destinationConversion)));
        	    		currency_destination_number.setText(destinationTotal);
        	    	}
        	    	else if (destinationCurrency.matches("") && originCurrency.matches(""))
        	    	{
        	    		Context context = getActivity().getApplicationContext();
        	        	  CharSequence text = "Please Enter Values!";
        	        	  int duration = Toast.LENGTH_SHORT;

        	        	  Toast toast = Toast.makeText(context, text, duration);
        	        	  toast.show();

        	    	}
        	    	else{
        	    		Context context = getActivity().getApplicationContext();
      	        	  CharSequence text = "Please Clear the Current Values!";
      	        	  int duration = Toast.LENGTH_SHORT;

      	        	  Toast toast = Toast.makeText(context, text, duration);
      	        	  toast.show();
        	    	}
        	    	
        	    }
        	});
          clear = (Button) view.findViewById(R.id.currency_clear);
          clear.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				currency_origin_number.setText("");
				currency_destination_number.setText("");
			}
		});
          currency_instructions = (TextView) view.findViewById(R.id.currency_instructions);
          currency_display = (TextView) view.findViewById(R.id.currency_display);
          LinearLayout rlayout = (LinearLayout) view.findViewById(R.id.currency_layout);
          rlayout.setOnClickListener(new View.OnClickListener() {
        	  @Override
        	  public void onClick(View v){
        		InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
      	    		imm.hideSoftInputFromWindow(currency_origin_number.getWindowToken(), 0);
        	  }
          });
          
          
          //the following code gets the origin and destination from this fragments activity, MainActivity
          MainActivity activity = (MainActivity) getActivity();
          String origin = activity.getOrigin();
          String destination = activity.getDestination();
          String originLabel = dbHelper.getOriginCurrencyType();
          String destinationLabel = dbHelper.getDestinationCurrencyType();
          
          //this code sets viewable text views to desired output
          currency_origin.setText(origin);
          currency_origin_label.setText(originLabel);
          currency_destination.setText(destination);
          currency_destination_label.setText(destinationLabel);
          currency_display.setText(origin + " - > " + destination);

          return view;
    }

}
